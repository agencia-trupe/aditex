<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinhasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('linhas')->insert([
            'id'     => 1,
            'capa'   => '',
            'slug'   => 'adesivos',
            'titulo' => 'Adesivos',
            'frase'  => '',
        ]);

        DB::table('linhas')->insert([
            'id'     => 2,
            'capa'   => '',
            'slug'   => 'agronegocio',
            'titulo' => 'Agronegócio',
            'frase'  => '',
        ]);

        DB::table('linhas')->insert([
            'id'     => 3,
            'capa'   => '',
            'slug'   => 'construcao-civil',
            'titulo' => 'Construção Civil',
            'frase'  => '',
        ]);

        DB::table('linhas')->insert([
            'id'     => 4,
            'capa'   => '',
            'slug'   => 'domissanitario',
            'titulo' => 'Domissanitário',
            'frase'  => '',
        ]);

        DB::table('linhas')->insert([
            'id'     => 5,
            'capa'   => '',
            'slug'   => 'pre-moldados',
            'titulo' => 'Pré-moldados',
            'frase'  => '',
        ]);

        DB::table('linhas')->insert([
            'id'     => 6,
            'capa'   => '',
            'slug'   => 'tintas',
            'titulo' => 'Tintas',
            'frase'  => '',
        ]);
    }
}
