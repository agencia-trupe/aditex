<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos')->insert([
            'capa'            => '',
            'nome'            => 'Aditex',
            'email_produtos'  => 'contato@trupe.net',
            'email_trab_conosco' => 'contato@trupe.net',
            'atendimento'     => 'Segunda à Sexta das 08h00 às 17h30',
            'filial_1'        => 'São Paulo',
            'telefone_1'      => '+55 (11) 3797 3400',
            'filial_2'        => 'Paraíba',
            'telefone_2'      => '+55 (83) 3245 3850',
            'email_vendas'    => 'vendas@aditex.com.br',
            'email_marketing' => 'marketing@aditex.com.br',
            'email_compras'   => 'compras@aditex.com.br',
            'email_rh'        => 'rh@aditex.com.br',
            'whatsapp'        => '+55 11 3797 3400',
        ]);
    }
}
