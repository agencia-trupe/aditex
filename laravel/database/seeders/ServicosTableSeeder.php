<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'id'        => 1,
            'capa'      => '',
            'slug'      => 'inicie-seu-negocio',
            'titulo'    => 'Inicie seu negócio',
            'subtitulo' => 'Assessoria técnica e comercial',
            'texto'     => '',
        ]);

        DB::table('servicos')->insert([
            'id'        => 2,
            'capa'      => '',
            'slug'      => 'cta',
            'titulo'    => 'CTA',
            'subtitulo' => 'Centros Técnicos Aditex, altamente equipados e gratuítos',
            'texto'     => '',
        ]);

        DB::table('servicos')->insert([
            'id'        => 3,
            'capa'      => '',
            'slug'      => 'treinamentos-e-palestras',
            'titulo'    => 'Treinamentos e Palestras',
            'subtitulo' => 'Ministrados para desenvolver e conscientizar o setor',
            'texto'     => '',
        ]);
    }
}
