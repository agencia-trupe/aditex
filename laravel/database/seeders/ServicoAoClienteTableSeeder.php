<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicoAoClienteTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('servico_ao_cliente')->insert([
            'capa'        => '',
            'frase_capa'  => 'A Aditex foi e continua sendo um canal importante para o desenvolvimento de nossos consumidores.',
            'titulo_capa' => 'Como podemos ajudar?',
            'texto'       => '',
            'frase_texto' => 'Se podemos iniciar juntos, também podemos crescer juntos.',
        ]);
    }
}
