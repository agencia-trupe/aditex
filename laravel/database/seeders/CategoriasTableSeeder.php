<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('categorias')->insert([
            'id'       => 1,
            'slug'     => 'polimeros',
            'titulo'   => 'Polímeros',
        ]);

        DB::table('categorias')->insert([
            'id'       => 2,
            'slug'     => 'celulosicos',
            'titulo'   => 'Celulósicos',
        ]);

        DB::table('categorias')->insert([
            'id'       => 3,
            'slug'     => 'especialidades',
            'titulo'   => 'Especialidades',
        ]);

        DB::table('categorias')->insert([
            'id'       => 4,
            'slug'     => 'pigmentos',
            'titulo'   => 'Pigmentos',
        ]);

        DB::table('categorias')->insert([
            'id'       => 5,
            'slug'     => 'cimento-branco',
            'titulo'   => 'Cimento Branco',
        ]);
    }
}
