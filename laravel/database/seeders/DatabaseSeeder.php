<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(ContatosTableSeeder::class);
        $this->call(HomeTableSeeder::class);
        $this->call(EmpresaTableSeeder::class);
        $this->call(ProdutosPaginaTableSeeder::class);
        $this->call(LinhasTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(AplicacoesTableSeeder::class);
        $this->call(ServicoAoClienteTableSeeder::class);
        $this->call(ServicosTableSeeder::class);
    }
}
