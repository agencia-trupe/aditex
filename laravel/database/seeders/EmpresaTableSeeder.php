<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa')->insert([
            'capa'                 => '',
            'texto_historia'       => '',
            'frase_estrutura'      => 'Logística facilitada ao longo do território nacional, permite alcançar a todos em qualquer localidade com agilidade e segurança.',
            'centros_distribuicao' => 'Amazonas|Paraíba|Pernambuco|Rio de Janeiro|Santa Catarina|São Paulo',
            'laboratorios'         => 'Amazonas|Paraíba|São Paulo',
            'escritorios'          => 'Manaus - AM|Cabedelo - PB|São Paulo - SP',
            'img_mapa'             => '',
            'titulo_certificado'   => 'A ADITEX é uma indústria CERTIFICADA',
            'img_certificado'      => '',
            'politica_qualidade'   => '',
            'objetivos_qualidade'  => '',
            'img_servicos'         => '',
            'titulo_servicos'      => 'Sua empresa focada em desenvolver e formular os melhores produtos',
            'frase_servicos'       => 'A ADITEX auxilia a sua empresa em todas as etapas para desenvolver produtos mais eficientes, mais econômicos, com ganho de desempenho e alta performance.',
            'btn_servicos'         => 'CONHEÇA TAMBÉM OS NOSSOS SERVIÇOS',
        ]);
    }
}
