<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AplicacoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('aplicacoes')->insert([
            'id'     => 1,
            'slug'   => 'argamassas-projetadas',
            'titulo' => 'Argamassas projetadas',
        ]);

        DB::table('aplicacoes')->insert([
            'id'     => 2,
            'slug'   => 'argamassas-de-rejuntamentos',
            'titulo' => 'Argamassas de rejuntamentos',
        ]);

        DB::table('aplicacoes')->insert([
            'id'     => 3,
            'slug'   => 'argamassas-hidrorrepelentes',
            'titulo' => 'Argamassas hidrorrepelentes',
        ]);

        DB::table('aplicacoes')->insert([
            'id'     => 4,
            'slug'   => 'argamassas-de-revestimentos',
            'titulo' => 'Argamassas de revestimentos',
        ]);

        DB::table('aplicacoes')->insert([
            'id'     => 5,
            'slug'   => 'revestimentos-com-gesso',
            'titulo' => 'Revestimentos com gesso',
        ]);

        DB::table('aplicacoes')->insert([
            'id'     => 6,
            'slug'   => 'tintas',
            'titulo' => 'Tintas',
        ]);

        DB::table('aplicacoes')->insert([
            'id'     => 7,
            'slug'   => 'texturas',
            'titulo' => 'Texturas',
        ]);
    }
}
