<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdutosPaginaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('produtos_pagina')->insert([
            'capa'        => 'bg-banner-produtos_20210720131357yyuutCHBdt.jpg',
            'titulo_capa' => 'Ampla gama de Aditivos Químicos',
            'frase'       => 'Projetados para melhorar a performance e qualidade, os aditivos foram desenvolvidos para atender todas as necessidades do mercado doméstico e da América Latina.',
        ]);
    }
}
