<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'frase_segmentos' => 'Pioneira e líder em aditivos químicos para argamassas, a Aditex também atende outros segmentos de mercado',
            'titulo_servicos' => 'Conheça nossos serviços, pensados para atender suas necessidades',
            'frase_servicos'  => 'Entenda como a Aditex pode ajudar a sua empresa por meio de serviços como acessoria especializada, desenvolvimento de produtos e muito mais.',
        ]);
    }
}
