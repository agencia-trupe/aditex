<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->id();
            $table->string('capa');
            $table->string('nome');
            $table->string('email_produtos');
            $table->string('email_trab_conosco');
            $table->string('atendimento');
            $table->string('filial_1');
            $table->string('telefone_1');
            $table->string('filial_2');
            $table->string('telefone_2');
            $table->string('email_vendas');
            $table->string('email_marketing');
            $table->string('email_compras');
            $table->string('email_rh');
            $table->string('whatsapp');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
