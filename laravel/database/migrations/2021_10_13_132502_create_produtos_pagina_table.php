<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosPaginaTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_pagina', function (Blueprint $table) {
            $table->id();
            $table->string('capa');
            $table->string('titulo_capa');
            $table->string('frase');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('produtos_pagina');
    }
}
