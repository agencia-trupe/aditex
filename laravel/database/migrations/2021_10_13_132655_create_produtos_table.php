<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('categoria_id')->constrained('categorias')->onDelete('cascade');
            $table->string('capa');
            $table->string('slug');
            $table->string('titulo');
            $table->string('subtitulo');
            $table->text('descricao');
            $table->text('embalagem')->nullable();
            $table->string('validade')->nullable();
            $table->text('armazenamento')->nullable();
            $table->json('linhas')->nullable();
            $table->json('aplicacoes')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
