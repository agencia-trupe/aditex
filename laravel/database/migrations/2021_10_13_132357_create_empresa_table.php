<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->id();
            $table->string('capa');
            $table->text('texto_historia');
            $table->string('frase_estrutura');
            $table->text('centros_distribuicao');
            $table->text('laboratorios');
            $table->text('escritorios');
            $table->string('img_mapa');
            $table->string('titulo_certificado');
            $table->string('img_certificado');
            $table->text('politica_qualidade');
            $table->text('objetivos_qualidade');
            $table->string('img_servicos');
            $table->string('titulo_servicos');
            $table->string('frase_servicos');
            $table->string('btn_servicos');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
