<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosArquivosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_arquivos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('produto_id')->constrained('produtos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_arquivo');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('produtos_arquivos');
    }
}
