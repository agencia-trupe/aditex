<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosInformacoesTable extends Migration
{
    public function up()
    {
        Schema::create('servicos_informacoes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('servico_id')->constrained('servicos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('titulo_info');
            $table->string('frase_info');
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('servicos_informacoes');
    }
}
