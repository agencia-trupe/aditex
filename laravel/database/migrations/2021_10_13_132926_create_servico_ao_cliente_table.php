<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicoAoClienteTable extends Migration
{
    public function up()
    {
        Schema::create('servico_ao_cliente', function (Blueprint $table) {
            $table->id();
            $table->string('capa');
            $table->string('frase_capa');
            $table->string('titulo_capa');
            $table->text('texto');
            $table->string('frase_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('servico_ao_cliente');
    }
}
