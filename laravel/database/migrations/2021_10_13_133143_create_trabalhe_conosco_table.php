<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrabalheConoscoTable extends Migration
{
    public function up()
    {
        Schema::create('trabalhe_conosco', function (Blueprint $table) {
            $table->id();
            $table->string('arquivo');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('trabalhe_conosco');
    }
}
