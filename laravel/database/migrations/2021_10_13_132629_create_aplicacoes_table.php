<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAplicacoesTable extends Migration
{
    public function up()
    {
        Schema::create('aplicacoes', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('titulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('aplicacoes');
    }
}
