<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasTable extends Migration
{
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->id();
            $table->date('data');
            $table->string('capa');
            $table->string('slug');
            $table->string('titulo');
            $table->string('frase');
            $table->text('texto');
            $table->text('texto_ver_mais')->nullable();
            $table->string('arquivo')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
