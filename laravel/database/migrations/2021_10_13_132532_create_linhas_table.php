<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinhasTable extends Migration
{
    public function up()
    {
        Schema::create('linhas', function (Blueprint $table) {
            $table->id();
            $table->string('capa');
            $table->string('slug');
            $table->string('titulo');
            $table->string('frase');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('linhas');
    }
}
