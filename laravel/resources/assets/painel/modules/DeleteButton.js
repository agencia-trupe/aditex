export default function DeleteButton() {
    $("body").on("click", ".btn-delete", function (event) {
        event.preventDefault();

        var form = $(this).closest("form"),
            _this = this,
            message = $(this).hasClass("btn-delete-multiple")
                ? "Deseja excluir todos os registros?"
                : "Deseja excluir o registro?";

        bootbox.confirm({
            closeButton: false,
            size: "small",
            backdrop: true,
            message: message,
            buttons: {
                cancel: {
                    label: "Cancelar",
                    className: "btn-secondary btn-sm",
                },
                confirm: {
                    label: '<i class="bi bi-trash-fill me-2"></i>Excluir',
                    className: "btn-danger btn-sm",
                },
            },
            callback: function (result) {
                if (result) {
                    if ($(_this).hasClass("btn-delete-link"))
                        return (window.location.href = $(_this)[0].href);
                    form.submit();
                }
            },
        });
    });
}
