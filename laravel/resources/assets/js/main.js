import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
    var urlPrevia = "";
    // var urlPrevia = "/previa-aditex";

    // HEADER HOME
    if (window.location.href == routeHome) {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 110) {
                $(".header-home").css("background-color", "#FFF");
            }
            if ($(window).scrollTop() === 0) {
                $(".header-home").css(
                    "background-color",
                    "rgba(255,255,255,0.7)"
                );
            }
        });
    }

    // HOME - banners
    $(".banners").cycle({
        slides: ".banner",
        fx: "fade",
        speed: 800,
        manualSpeed: 100,
        next: ".cycle-pager",
    });

    // PRODUTOS-LINHA - removendo margin-right 4º elemento
    $(
        "section.produtos-linha .link-produto:nth-child(4n), section.produtos-filtro .link-produto:nth-child(4n)"
    ).css("margin-right", "0");

    // PRODUTOS-SHOW - removendo margin-right 5º elemento - aplicacoes
    if ($(window).width() >= 1260) {
        $("section.produtos-show .aplicacao:nth-child(5n)").css(
            "margin-right",
            "0"
        );
    }

    // SERVIÇOS - IMAGENS - fancybox
    $(".fancybox").fancybox({
        padding: 0,
        prevEffect: "fade",
        nextEffect: "fade",
        closeBtn: false,
        openEffect: "elastic",
        openSpeed: "150",
        closeEffect: "elastic",
        closeSpeed: "150",
        helpers: {
            title: {
                type: "outside",
                position: "top",
            },
            overlay: {
                css: {
                    background: "rgba(132, 134, 136, .88)",
                },
            },
        },
        fitToView: false,
        autoSize: false,
        beforeShow: function () {
            this.maxWidth = "90%";
            this.maxHeight = "90%";
        },
    });

    // NOTICIAS - btn ver mais
    var itensNoticias = $("section.noticias .link-noticia");
    var spliceItensQuantidade = 4;

    if (itensNoticias.length <= spliceItensQuantidade) {
        $(".btn-mais-noticias").hide();
    }

    var setDivNoticias = function () {
        var spliceItens = itensNoticias.splice(0, spliceItensQuantidade);
        $("section.noticias .demais-noticias").append(spliceItens);
        $(spliceItens).show();
        if (itensNoticias.length <= 0) {
            $(".btn-mais-noticias").hide();
        }
    };

    $(".btn-mais-noticias").click(function (e) {
        e.preventDefault();
        setDivNoticias();
    });

    $("section.noticias .link-noticia").hide();
    setDivNoticias();

    // NOTICIAS-SHOW - ver mais texto
    $("section.noticias-show .link-ver-mais").click(function () {
        $("section.noticias-show .texto-ver-mais").css("display", "block");

        var x = $("section.noticias-show .texto-ver-mais").position();
        $("html, body").animate({ scrollTop: x.top }, 800);

        $(this).css("visibility", "hidden");
    });

    // CONTATO - exibindo arquivo selecionado
    $("section.contato .arquivo-selecionado").hide();
    $("#files").change(function () {
        var filename = $(this)[0].files[0].name;
        $("section.contato .arquivo-selecionado").html(filename).show();
    });

    // HEADER - busca
    $("nav .link-busca").click(function (e) {
        e.preventDefault();
        $(".busca-header").show();
        e.stopPropagation();
    });
    $(".busca-header").click(function (e) {
        e.stopPropagation();
    });
    if ($(window).width() > 800) {
        $(window).scroll(function () {
            $(".busca-header").hide();
        });
    }
    $(document).click(function () {
        $(".busca-header").hide();
    });
    $(".btn-fechar-busca").click(function (e) {
        e.preventDefault();
        $(".busca-header").hide();
    });

    // HOME - filtro de produtos
    $("select#filtroCategorias").on("change", function () {
        var categoriaSelected = $("select#filtroCategorias").val();
        if (categoriaSelected == "") {
            categoriaSelected = 0;
        }
        console.log(categoriaSelected);

        var url = urlPrevia + "/filtro/" + categoriaSelected;

        $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $("#filtroAplicacoes").html("");
                var option =
                    "<option value='' selected>Por APLICAÇÃO no Mercado</option>";
                $("#filtroAplicacoes").append(option);

                data.aplicacoes.forEach((aplicacao) => {
                    var optionAplicacoes =
                        "<option value='" +
                        aplicacao.slug +
                        "'>" +
                        aplicacao.titulo +
                        "</option>";
                    $("#filtroAplicacoes").append(optionAplicacoes);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });

    // HEADER - filtro de produtos (BUSCA)
    $("select#filtroCategoriasBusca").on("change", function () {
        var categoriaSelected = $("select#filtroCategoriasBusca").val();
        if (categoriaSelected == "") {
            categoriaSelected = 0;
        }
        console.log(categoriaSelected);

        var url = urlPrevia + "/filtro/" + categoriaSelected;
        getProdutosFiltroBusca(url);
    });

    var getProdutosFiltroBusca = function (url) {
        $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $("#filtroAplicacoesBusca").html("");
                var option =
                    "<option value='' selected>Por APLICAÇÃO no Mercado</option>";
                $("#filtroAplicacoesBusca").append(option);

                data.aplicacoes.forEach((aplicacao) => {
                    var optionAplicacoes =
                        "<option value='" +
                        aplicacao.slug +
                        "'>" +
                        aplicacao.titulo +
                        "</option>";
                    $("#filtroAplicacoesBusca").append(optionAplicacoes);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    };

    // HOME - filtro após clique
    $("#btnFiltroBuscar").on("click", function (e) {
        e.preventDefault();

        var categoriaSelected = $("select#filtroCategorias").val();
        var aplicacaoSelected = $("select#filtroAplicacoes").val();

        if (categoriaSelected == "") {
            categoriaSelected = 0;
        }
        if (aplicacaoSelected == "") {
            aplicacaoSelected = 0;
        }

        console.log(categoriaSelected, aplicacaoSelected);
        window.location.href =
            window.location.origin +
            urlPrevia +
            "/produtos/filtro/" +
            categoriaSelected +
            "/" +
            aplicacaoSelected;
    });

    // HEADER - filtro após clique
    $("#btnBuscaFiltroBuscar").on("click", function (e) {
        e.preventDefault();

        var categoriaSelected = $("select#filtroCategoriasBusca").val();
        var aplicacaoSelected = $("select#filtroAplicacoesBusca").val();

        if (categoriaSelected == "") {
            categoriaSelected = 0;
        }
        if (aplicacaoSelected == "") {
            aplicacaoSelected = 0;
        }

        console.log(categoriaSelected, aplicacaoSelected);
        window.location.href =
            window.location.origin +
            urlPrevia +
            "/produtos/filtro/" +
            categoriaSelected +
            "/" +
            aplicacaoSelected;
    });

    // Whatsapp floating
    $("#chatWhatsapp").floatingWhatsApp({
        phone: whatsappNumero,
        popupMessage: "Seja muito bem-vindo(a) à ADITEX. Como podemos ajudar?",
        showPopup: true,
        autoOpen: false,
        headerTitle:
            '<div class="img-wpp"><img src="' +
            imgMarcaWhatsapp +
            '" alt=""></div>' +
            '<div class="textos"><p class="titulo">ADITEX</p><p class="frase">Responderemos o mais breve possível.</p></div>',
        headerColor: "#25D366",
        size: "60px",
        position: "right",
        linkButton: true,
    });

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
