@extends('frontend.layout.template')

@section('content')

<section class="produtos-show">

    <div class="capa-pagina">
        <div class="capa-infos">
            <div class="linha-categoria">
                <div class="textos">
                    <p class="produtos">PRODUTOS</p>
                    <a href="{{ route('produtos.filtro', [$categoria->slug, '0']) }}" class="link-categoria">{{ $categoria->titulo }}</a>
                    <div class="linhas-capa">
                        @foreach($linhas as $linha)
                        <a href="{{ route('produtos.linha', ['linha_slug' => $linha->slug]) }}" class="link-linha">{{ $linha->titulo }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="produto">
                <h2 class="titulo-produto">{{ $produto->titulo }}</h2>
                <p class="subtitulo-produto">{{ $produto->subtitulo }}</p>
            </div>
        </div>
        <div class="capa-produto">
            <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" class="img-produto" alt="{{ $produto->titulo }}">
        </div>
    </div>

    <div class="dados-produto">

        <div class="col-left">

            <div class="descricao">{!! $produto->descricao !!}</div>

            @if($produto->embalagem)
            <h2 class="titulo-embalagem">EMBALAGEM</h2>
            <div class="embalagem">{!! $produto->embalagem !!}</div>
            @endif

            @if($produto->validade)
            <h2 class="titulo-validade">VALIDADE</h2>
            <p class="validade">{{ $produto->validade }}</p>
            @endif

            @if($produto->armazenamento)
            <h2 class="titulo-armazenamento">ARMAZENAMENTO</h2>
            <div class="armazenamento">{!! $produto->armazenamento !!}</div>
            @endif

            @if(count($arquivos) > 0)
            <div class="downloads" id="arquivos">
                <h2 class="titulo-downloads">DOWNLOADS</h2>
                @foreach($arquivos as $arquivo)
                <a href="{{ 'http://www.aditex.com.br/assets/arquivos/produtos/'.$arquivo->arquivo }}" target="_blank" class="link-arquivo">
                    <div class="pdf">
                        <img src="{{ asset('assets/img/layout/icone-PDF.svg') }}" alt="" class="img-pdf">
                    </div>
                    <p class="titulo-arquivo">{{ $arquivo->titulo_arquivo }}</p>
                </a>
                @endforeach
            </div>
            @endif
        </div>

        <div class="col-right">

            @if(isset($aplicacoes) && (count($aplicacoes) > 0))
            <h2 class="titulo-aplicacoes">APLICAÇÕES</h2>
            <div class="aplicacoes">
                @foreach($aplicacoes as $aplicacao)
                <p class="aplicacao">{{ $aplicacao->titulo }}</p>
                @endforeach
            </div>
            <p class="obs-aplicacoes">OBS: Para aplicações diferentes das mencionadas aqui, entre em contato conosco.</p>
            @endif

            <div class="mais-informacoes">
                <h2 class="titulo">MAIS INFORMAÇÕES</h2>
                <p class="solicite">Solicite seu orçamento</p>
                <form action="{{ route('produtos.mais-informacoes', $produto->id) }}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                    <input type="text" name="empresa" placeholder="empresa" value="{{ old('empresa') }}">
                    <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                    <div class="opt-in-comunicados">
                        <input type="checkbox" id="optInComunicados" name="aceito" value="1">
                        <p class="texto-opt-in">Aceito receber comunicados da Aditex sobre produtos e sobre a empresa.</p>
                    </div>
                    <button type="submit" class="btn-contato btn-mais-informacoes-opt-in">
                        ENVIAR
                        <img src="{{ asset('assets/img/layout/seta-fina-branco.svg') }}" alt="" class="img-setinha">
                    </button>

                    @if($errors->any())
                    <div class="flash flash-erro">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('enviado'))
                    <div class="flash flash-sucesso">
                        <p>Mensagem enviada com sucesso!</p>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

</section>

@endsection