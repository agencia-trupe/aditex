@extends('frontend.layout.template')

@section('content')

<section class="produtos">

    <div class="capa-produtos" style="background-image: url({{ asset('assets/img/produtos/'.$pagina->capa) }})">
        <p class="titulo-capa">{{ $pagina->titulo_capa }}</p>
    </div>

    <p class="frase-produtos">{{ $pagina->frase }}</p>
    <p class="conheca">CONHEÇA NOSSA LINHA DE PRODUTOS:</p>

    <div class="linhas-categorias">
        @foreach($linhas as $linha)
        <div class="linha">
            <a href="{{ route('produtos.linha', ['linha_slug' => $linha->slug]) }}" class="link-linha">
                <img src="{{ asset('assets/img/produtos/linhas/miniaturas/'.$linha->capa) }}" class="img-linha" alt="{{ $linha->titulo }}">
                <p class="titulo-linha">{{ $linha->titulo }}</p>
            </a>
            <div class="categorias">
                @foreach($categorias as $categoria)
                @if($categoria->linha_id == $linha->id)
                <a href="{{ route('produtos.linha.categoria', ['linha_slug' => $linha->slug, 'categoria_slug' => $categoria->slug]) }}" class="link-categoria" id="{{$linha->id}}-{{$categoria->slug}}">{{ $categoria->titulo }}</a>
                @endif
                @endforeach
            </div>
        </div>
        @endforeach
    </div>

</section>

@endsection