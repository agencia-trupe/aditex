@extends('frontend.layout.template')

@section('content')

<section class="servico-ao-cliente">

    <div class="capa-servicos" style="background-image: url({{ asset('assets/img/servico-ao-cliente/'.$pagina->capa) }})">
        <div class="overlay-gradiente">
            <p class="frase-capa">{{ $pagina->frase_capa }}</p>
            <p class="titulo-capa">{{ $pagina->titulo_capa }}</p>
        </div>
    </div>

    <div class="infos-servicos">
        <div class="texto-servicos">{!! $pagina->texto !!}</div>
        <p class="frase-texto">{{ $pagina->frase_texto }}</p>
    </div>

    <div class="servicos">
        @foreach($servicos as $servico)
        <a href="{{ route('servico-ao-cliente.show', $servico->slug) }}" class="link-servico {{$servico->slug}}">
            <div class="icone"></div>
            <div class="textos-servico">
                <p class="titulo">{{ $servico->titulo }}</p>
                <p class="subtitulo">{{ $servico->subtitulo }}</p>
            </div>
            <img src="{{ asset('assets/img/layout/seta-fina.svg') }}" alt="" class="img-setinha">
        </a>
        @endforeach
    </div>

</section>

@endsection