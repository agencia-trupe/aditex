@extends('frontend.layout.template')

@section('content')

<section class="noticias-show">

    <div class="capa-e-noticias">
        <img src="{{ asset('assets/img/noticias/capas/'.$noticiaShow->capa) }}" class="img-capa" alt="{{ $noticiaShow->titulo }}">
        <hr class="linha-noticias">
        <div class="noticias">
            <h2 class="mais-noticias">MAIS NOTÍCIAS</h2>
            <div class="links-noticias">
                @foreach($noticias as $noticia)
                <a href="{{ route('noticias.showNoticia', $noticia->slug) }}" class="link-noticia">
                    <div class="capa-noticia">
                        <img src="{{ asset('assets/img/noticias/'.$noticia->capa) }}" class="img-noticia" alt="{{ $noticia->titulo }}">
                    </div>
                    <div class="textos-noticia">
                        <p class="titulo">{{ $noticia->titulo }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            <a href="{{ route('noticias') }}" class="btn-voltar-noticias">« voltar para home de notícias</a>
        </div>
    </div>

    <div class="dados-noticia">
        <div class="bg-titulo">
            <h2 class="titulo-pagina">NOTÍCIAS</h2>
            <h2 class="titulo-noticia">{{ $noticiaShow->titulo }}</h2>
        </div>
        <div class="texto">{!! $noticiaShow->texto !!}</div>
        @if(isset($noticiaShow->texto_ver_mais))
        <div class="texto-ver-mais" style="display: none;">{!! $noticiaShow->texto_ver_mais !!}</div>
        <button type="button" class="link-ver-mais">ver mais +</button>
        @endif
        @if(isset($noticiaShow->arquivo))
        <a href="{{ 'http://www.aditex.com.br/assets/arquivos/'.$noticiaShow->arquivo }}" target="_blank" class="link-download">DOWNLOAD DO ARQUIVO</a>
        @endif
    </div>

</section>

@endsection