@extends('frontend.layout.template')

@section('content')

<section class="produtos-filtro">

    <div class="capa-filtro" style="background-image: url({{ asset('assets/img/layout/bg-cabecalho-resultado-busca.jpg') }})">
        <p class="titulo-capa">RESULTADO DA BUSCA DE PRODUTOS</p>
    </div>

    <div class="produtos-categoria">
        @if(isset($categorias))
        @foreach($categorias as $categoria)
        <p class="categoria" id="{{ $categoria->slug }}">{{ $categoria->titulo }}</p>
        <div class="produtos">
            @foreach($produtos as $produto)
            @if($produto->categoria_id == $categoria->id)
            @php $linha = $linhas->where('produto_id', $produto->id)->first(); @endphp
            <a href="{{ route('produtos.showProduto', ['produto_slug' => $produto->slug]) }}" class="link-produto">
                <img src="{{ asset('assets/img/produtos/miniaturas/'.$produto->capa) }}" class="img-produto" alt="{{ $produto->titulo }}">
                <div class="sobre-produto">
                    <h2 class="titulo">{{ $produto->titulo }}</h2>
                    <p class="subtitulo">{{ $produto->subtitulo }}</p>
                </div>
                <hr class="linha-hover">
            </a>
            @endif
            @endforeach
        </div>
        @endforeach
        @else
        <p class="categoria" id="{{ $categoria->slug }}">{{ $categoria->titulo }}</p>
        <div class="produtos">
            @foreach($produtos as $produto)
            @php $linha = $linhas->where('produto_id', $produto->id)->first(); @endphp
            <a href="{{ route('produtos.showProduto', ['produto_slug' => $produto->slug]) }}" class="link-produto">
                <img src="{{ asset('assets/img/produtos/miniaturas/'.$produto->capa) }}" class="img-produto" alt="{{ $produto->titulo }}">
                <div class="sobre-produto">
                    <h2 class="titulo">{{ $produto->titulo }}</h2>
                    <p class="subtitulo">{{ $produto->subtitulo }}</p>
                </div>
                <hr class="linha-hover">
            </a>
            @endforeach
        </div>
        @endif
    </div>

</section>

@endsection