@extends('frontend.layout.template')

@section('content')

<section class="servico-ao-cliente-show">

    <div class="capa-servico" style="background-image: url({{ asset('assets/img/servicos/'.$servico->capa) }})"></div>

    <div class="dados-servico">
        <div class="left">
            <div class="servico {{ $servico->slug }}">
                <div class="icone"></div>
                <div class="dados">
                    <h2 class="titulo-servico">{{ $servico->titulo }}</h2>
                    <p class="subtitulo-servico">{{ $servico->subtitulo }}</p>
                </div>
            </div>
            <div class="imagens">
                @foreach($imagens as $imagem)
                <a href="{{ asset('assets/img/servicos/imagens/originais/'.$imagem->imagem) }}" class="link-imagem fancybox" rel="servico">
                    <img src="{{ asset('assets/img/servicos/imagens/'.$imagem->imagem) }}" class="img-servico" alt="">
                </a>
                @endforeach
            </div>
        </div>
        <div class="right">
            <div class="texto-servico">{!! $servico->texto !!}</div>
            @foreach($informacoes as $info)
            @if($info->link)
            <a href="" class="info-dados link-info" id="info-{{$info->id}}">
                <p class="titulo-info"><span>»</span>{{ $info->titulo_info }}</p>
                <p class="frase-info">{{ $info->frase_info }}</p>
            </a>
            @else
            <div class="info-dados" id="info-{{$info->id}}">
                <p class="titulo-info"><span>»</span>{{ $info->titulo_info }}</p>
                <p class="frase-info">{{ $info->frase_info }}</p>
            </div>
            @endif
            @endforeach
        </div>
    </div>

    <div class="servicos">
        @foreach($servicos as $servico)
        <a href="{{ route('servico-ao-cliente.show', $servico->slug) }}" class="link-servico {{$servico->slug}}">
            <div class="icone"></div>
            <div class="textos-servico">
                <p class="titulo">{{ $servico->titulo }}</p>
                <p class="subtitulo">{{ $servico->subtitulo }}</p>
            </div>
            <img src="{{ asset('assets/img/layout/seta-fina.svg') }}" alt="" class="img-setinha">
        </a>
        @endforeach
    </div>
    
</section>

@endsection