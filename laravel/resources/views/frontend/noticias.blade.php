@extends('frontend.layout.template')

@section('content')

<section class="noticias">

    <a href="{{ route('noticias.showNoticia', $destaque->slug) }}" class="link-destaque">
        <div class="capa-destaque">
            <img src="{{ asset('assets/img/noticias/capas/'.$destaque->capa) }}" class="img-capa" alt="{{ $destaque->titulo }}">
        </div>
        <div class="dados-destaque">
            <h2 class="titulo-pagina">NOTÍCIAS</h2>
            <h4 class="titulo-destaque">{{ $destaque->titulo }}</h4>
            <p class="frase-destaque">{{ $destaque->frase }}</p>
        </div>
    </a>

    <div class="demais-noticias">
        @foreach($noticias as $noticia)
        <a href="{{ route('noticias.showNoticia', ['noticia_slug' => $noticia->slug]) }}" class="link-noticia">
            <div class="capa-noticia">
                <img src="{{ asset('assets/img/noticias/'.$noticia->capa) }}" class="img-capa" alt="{{ $noticia->titulo }}">
            </div>
            <div class="textos-noticia">
                <p class="titulo-noticia">{{ $noticia->titulo }}</p>
                <p class="frase-noticia">{{ $noticia->frase }}</p>
            </div>
        </a>
        @endforeach
    </div>
    <a href="" class="btn-mais-noticias">ver mais »</a>

</section>

@endsection