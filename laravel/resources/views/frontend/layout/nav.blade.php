<div class="itens-menu">
    <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa*')) class="link-nav active" @endif class="link-nav">EMPRESA</a>
    <a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos*')) class="link-nav active" @endif class="link-nav">PRODUTOS</a>
    <a href="{{ route('servico-ao-cliente') }}" @if(Tools::routeIs('servico-ao-cliente*')) class="link-nav active" @endif class="link-nav">SERVIÇO AO CLIENTE</a>
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-nav active" @endif class="link-nav">CONTATO</a>
    <a href="#" @if(Tools::routeIs('busca*')) class="link-busca active" @endif class="link-busca"></a>
    <div class="cotacao">
        <p class="dolar">DÓLAR | <span class="valor-dolar">{{ $valorCotacao }}</span></p>
        <p class="infos-cotacao">EUA | {{ $dataCotacao }} (PTAX)</p>
    </div>
</div>