<footer>

    <div class="footer">
        <div class="links-principais">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home*')) class="link-footer active" @endif class="link-footer">HOME</a>
            <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa*')) class="link-footer active" @endif class="link-footer">EMPRESA</a>
            <a href="{{ route('noticias') }}" @if(Tools::routeIs('noticias*')) class="link-footer active" @endif class="link-footer">NOTÍCIAS</a>
        </div>
        <div class="links-produtos">
            <a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos*')) class="link-footer active" @endif class="link-footer">PRODUTOS</a>
            @foreach($linhas as $linha)
            <a href="{{ route('produtos.linha', ['linha_slug' => $linha->slug]) }}" class="sublink-footer"><span>•</span>{{ $linha->titulo }}</a>
            @endforeach
        </div>
        <div class="links-servicos">
            <a href="{{ route('servico-ao-cliente') }}" @if(Tools::routeIs('servico-ao-cliente*')) class="link-footer active" @endif class="link-footer">SERVIÇO AO CLIENTE</a>
            @foreach($servicos as $servico)
            <a href="{{ route('servico-ao-cliente.show', $servico->slug) }}" class="sublink-footer"><span>•</span>{{ $servico->titulo }}</a>
            @endforeach
        </div>
        <div class="links-secundarios">
            <a href="{{ route('contato') }}" class="link-footer">CONTATO</a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="sublink-footer active" @endif class="sublink-footer"><span>•</span>Fale conosco</a>
            <a href="{{ route('contato').'#trabalheConosco' }}" class="sublink-footer"><span>•</span>Trabalhe conosco</a>
            <a href="{{ route('politica-de-privacidade') }}" @if(Tools::routeIs('politica-de-privacidade*')) class="sublink-footer link-politica active" @endif class="sublink-footer link-politica"><span>•</span>Política de Privacidade</a>
        </div>
        <div class="dados-empresa">
            <a href="{{ route('home') }}" class="logo-footer" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-aditex-branco.svg') }}" alt="" class="img-logo"></a>
            <p class="filial1">{{ $contato->filial_1 }}</p>
            @php $telefone1 = str_replace(")", "", str_replace("(", "", str_replace(" ", "", $contato->telefone_1))); @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $telefone1 }}" class="telefone" target="_blank">{{ str_replace("(", "", str_replace(")", "", $contato->telefone_1)) }}</a>
            <p class="filial2">{{ $contato->filial_2 }}</p>
            @php $telefone2 = str_replace(")", "", str_replace("(", "", str_replace(" ", "", $contato->telefone_2))); @endphp
            <a href="tel:{{ $telefone2 }}" class="telefone">{{ str_replace("(", "", str_replace(")", "", $contato->telefone_2)) }}</a>
            <p class="atendimento">{{ $contato->atendimento }}</p>
        </div>
    </div>

    <div class="direitos">
        <p class="empresa">© {{ date('Y') }} {{ $config->title }} • Todos os direitos reservados | </p>
        <a href="http://www.trupe.net" target="_blank" class="link-trupe">Criação de sites:</a>
        <a href="http://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>
    </div>

</footer>