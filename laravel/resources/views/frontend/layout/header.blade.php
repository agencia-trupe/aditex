@if(Tools::routeIs('home'))
<header class="header-home">
    @else
    <header class="header-geral">
        @endif
        <hr class="linha-topo">
        <div class="centralizado">
            <a href="{{ route('home') }}" class="logo-header" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-aditex.svg') }}" alt="" class="img-logo"></a>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines internas"></span>
            </button>
            <nav>
                @include('frontend.layout.nav')
            </nav>
        </div>
    </header>

    <div class="busca-header" style="display: none;">
        <a href="" class="btn-fechar-busca"><img src="{{ asset('assets/img/layout/x-fechar.svg') }}" alt="" class="img-fechar"></a>
        <form action="{{ route('busca.palavra') }}" method="post" class="form-palavra-chave">
            {!! csrf_field() !!}
            <p class="frase-busca">BUSQUE POR PALAVRA-CHAVE</p>
            <input type="text" name="busca" class="input-busca" placeholder="">
            <button type="submit" class="btn-buscar">BUSCAR</button>
        </form>
        <div class="filtro">
            <p class="frase-filtro">ENCONTRE O PRODUTO ADEQUADO PARA SEU NEGÓCIO</p>
            <select name="categoria_id" class="categorias-filtro" id="filtroCategoriasBusca">
                <option value="" selected>Por TIPO de Produto</option>
                @foreach($categoriasFiltro as $categoria)
                <option value="{{ $categoria->slug }}">{{ $categoria->titulo }}</option>
                @endforeach
            </select>
            <select name="aplicacao_id" class="aplicacoes-filtro" id="filtroAplicacoesBusca">
                <option value="" selected>Por APLICAÇÃO no Mercado</option>
                @foreach($aplicacoes as $aplicacao)
                <option value="{{ $aplicacao->slug }}">{{ $aplicacao->titulo }}</option>
                @endforeach
            </select>
            <a href="" class="btn-filtro-buscar" id="btnBuscaFiltroBuscar">BUSCAR</a>
        </div>
        <div class="linhas-categorias">
            @foreach($linhas as $linha)
            <div class="linha">
                <a href="{{ route('produtos.linha', ['linha_slug' => $linha->slug]) }}" class="link-linha">{{ $linha->titulo }}</a>
                <div class="categorias">
                    @foreach($categorias as $categoria)
                    @if($categoria->linha_id == $linha->id)
                    <a href="{{ route('produtos.linha', ['linha_slug' => $linha->slug]) .'#'.$categoria->slug }}" class="link-categoria" id="{{$categoria->slug}}">{{ $categoria->titulo }}</a>
                    @endif
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>