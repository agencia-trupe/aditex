@extends('frontend.layout.template')

@section('content')

<section class="contato">

    <div class="capa-contato">
        <div class="capa">
            <img src="{{ asset('assets/img/contatos/'.$contato->capa) }}" class="img-capa" alt="">
        </div>
        <div class="dados-capa">
            <p class="fale-conosco">Alguma dúvida ou sugestão? Fale com a gente</p>
            <div class="contatos">
                <div class="dados-left">
                    <p class="atendimentos">Nossas Centrais de Atendimento</p>
                    <p class="horarios">{{ $contato->atendimento }}</p>
                    <div class="filial-1">
                        <img src="{{ asset('assets/img/layout/icone-contato.svg') }}" alt="" class="icone-fone">
                        <div class="local-telefone">
                            <p class="local">{{ $contato->filial_1 }}</p>
                            @php $telefone1 = str_replace(")", "", str_replace("(", "", str_replace(" ", "", $contato->telefone_1))); @endphp
                            <a href="https://api.whatsapp.com/send?phone={{ $telefone1 }}" class="telefone" target="_blank">{{ $contato->telefone_1 }}</a>
                        </div>
                    </div>
                    <div class="filial-2">
                        <img src="{{ asset('assets/img/layout/icone-contato.svg') }}" alt="" class="icone-fone">
                        <div class="local-telefone">
                            <p class="local">{{ $contato->filial_2 }}</p>
                            @php $telefone2 = str_replace(")", "", str_replace("(", "", str_replace(" ", "", $contato->telefone_2))); @endphp
                            <a href="tel:{{ $telefone2 }}" class="telefone">{{ $contato->telefone_2 }}</a>
                        </div>
                    </div>
                </div>
                <div class="dados-right">
                    <p class="frase-emails">Se preferir, envie um e-mail e, em breve, retornaremos.</p>
                    <a href="mailto:{{ $contato->email_vendas }}" class="email-vendas">
                        <img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt="" class="icone-email">
                        VENDAS <span>·</span> {{ $contato->email_vendas }}
                    </a>
                    <a href="mailto:{{ $contato->email_marketing }}" class="email-vendas">
                        <img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt="" class="icone-email">
                        MARKETING <span>·</span> {{ $contato->email_marketing }}
                    </a>
                    <a href="mailto:{{ $contato->email_compras }}" class="email-vendas">
                        <img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt="" class="icone-email">
                        COMPRAS <span>·</span> {{ $contato->email_compras }}
                    </a>
                    <a href="mailto:{{ $contato->email_rh }}" class="email-vendas">
                        <img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt="" class="icone-email">
                        RH <span>·</span> {{ $contato->email_rh }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="form-contato-externo">
        <script type="text/javascript" src=https://form.jotform.com/jsform/212216595870661></script>
    </div>

    <div class="trabalhe-conosco" id="trabalheConosco">
        <h2 class="titulo-trabalhe-conosco">TRABALHE CONOSCO</h2>
        <p class="frase1">A Aditex é uma empresa que valoriza o trabalho e o desenvolvimento humano.</p>
        <p class="frase2">Interessado em fazer parte de nossa equipe?</p>
        <p class="frase3">Anexe o seu currículo.</p>
        <form action="{{ route('trabalhe-conosco.post') }}" method="POST" enctype="multipart/form-data" class="form-trabalhe-conosco">
            {!! csrf_field() !!}
            <label for="files" class="btn-anexar">Envie-nos o seu currículo</label>
            <input type="file" id="files" name="arquivo" style="visibility:hidden;position:absolute;">
            <p class="arquivo-selecionado"></p>
            <button type="submit" class="btn-enviar">ENVIAR</button>
        </form>
        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            <p>Mensagem enviada com sucesso!</p>
        </div>
        @endif
        <p class="envie-curriculo">Ou envie seu currículo para:
            <a href="mailto:{{ $contato->email_rh }}" class="email">
                <img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt="" class="icone-email">
                {{ $contato->email_rh }}
            </a>
        </p>
    </div>
</section>

@endsection