@extends('frontend.layout.template')

@section('content')

<section class="produtos-linha">

    <div class="capa-pagina">
        <div class="textos-linha">
            <p class="produtos">PRODUTOS</p>
            <a href="{{ route('produtos.linha', ['linha_slug' => $linha->slug]) }}" class="link-linha">{{ $linha->titulo }}</a>
            @if(isset($categoria))
            <a href="{{ route('produtos.filtro', [$categoria->slug, '0']) }}" class="link-categoria">{{ $categoria->titulo }}</a>
            @endif
            <p class="frase-linha">{{ $linha->frase }}</p>
        </div>
        <div class="capa-linha">
            <img src="{{ asset('assets/img/produtos/linhas/'.$linha->capa) }}" class="img-linha" alt="{{ $linha->titulo }}">
        </div>
    </div>

    <div class="produtos-categoria">
        @if(isset($categorias))
        @foreach($categorias as $categoria)
        <p class="categoria" id="{{$linha->id}}-{{$categoria->slug}}">{{ $categoria->titulo }}</p>
        <div class="produtos">
            @foreach($produtos as $produto)
            @if($produto->categoria_id == $categoria->id)
            <a href="{{ route('produtos.showProduto', ['produto_slug' => $produto->slug]) }}" class="link-produto">
                <img src="{{ asset('assets/img/produtos/miniaturas/'.$produto->capa) }}" class="img-produto" alt="{{ $produto->titulo }}">
                <div class="sobre-produto">
                    <h2 class="titulo">{{ $produto->titulo }}</h2>
                    <p class="subtitulo">{{ $produto->subtitulo }}</p>
                </div>
                <hr class="linha-hover">
            </a>
            @endif
            @endforeach
        </div>
        @endforeach
        @else
        <p class="categoria" id="{{$linha->id}}-{{$categoria->slug}}">{{ $categoria->titulo }}</p>
        <div class="produtos">
            @foreach($produtos as $produto)
            <a href="{{ route('produtos.showProduto', ['produto_slug' => $produto->slug]) }}" class="link-produto">
                <img src="{{ asset('assets/img/produtos/miniaturas/'.$produto->capa) }}" class="img-produto" alt="{{ $produto->titulo }}">
                <div class="sobre-produto">
                    <h2 class="titulo">{{ $produto->titulo }}</h2>
                    <p class="subtitulo">{{ $produto->subtitulo }}</p>
                </div>
                <hr class="linha-hover">
            </a>
            @endforeach
        </div>
        @endif
    </div>

</section>

@endsection