@extends('frontend.layout.template')

@section('content')

<section class="busca">

    <div class="capa-busca" style="background-image: url({{ asset('assets/img/layout/bg-cabecalho-resultado-busca.jpg') }})">
        <p class="titulo-capa">RESULTADO DA BUSCA</p>
    </div>

    <h2 class="titulo-busca">TERMO DA BUSCA: <span>{{ $termo }}</span></h2>

    <div class="resultados">
        @if(count($resultados))
        @foreach($resultados as $secoes)
        @foreach($secoes as $resultado)
        <a href="{{ $resultado['link'] }}" class="link-resultado">
            <p class="menu-titulo">{{ $resultado['menu'] . ' | ' . $resultado['titulo'] }}</p>
            <p class="texto-resultado">{!! Tools::str_words(strip_tags($resultado['frase'], '<span>'), 30) !!}</p>
        </a>
        @endforeach
        @endforeach
        @else
        <div class="nenhum-resultado">
            Nenhum resultado encontrado para a busca: <strong>{{$termo}}</strong>
        </div>
        @endif
    </div>

</section>

@endsection