@extends('frontend.layout.template')

@section('content')

<section class="empresa">

    <div class="capa-empresa" style="background-image: url({{ asset('assets/img/empresa/'.$empresa->capa) }})"></div>

    <div class="historia">
        <h2 class="titulo">NOSSA HISTÓRIA</h2>
        <div class="texto">{!! $empresa->texto_historia !!}</div>
    </div>

    <div class="informacoes">
        @php
        $count = 0;
        $right = true;
        @endphp
        @foreach($informacoes as $info)
        @if($count == 0)
        @if($right)
        <div class="info info-right" id="info-{{$info->id}}">
            <img src="{{ asset('assets/img/empresa/'.$info->imagem) }}" alt="" class="img-info">
            <div class="textos-info">
                <p class="titulo">{{ $info->titulo }}</p>
                <div class="texto">{!! $info->texto !!}</div>
            </div>
            @else
            <div class="info info-left">
                <div class="textos-info">
                    <p class="titulo">{{ $info->titulo }}</p>
                    <div class="texto">{!! $info->texto !!}</div>
                </div>
                <img src="{{ asset('assets/img/empresa/'.$info->imagem) }}" alt="" class="img-info">
                @endif
                @endif
                @php
                $count++;
                @endphp
                @if($count == 1)
            </div>
            @php
            $count = 0;
            $right = !$right;
            @endphp
            @endif
            @endforeach
        </div>
    </div>

    <div class="estrutura">
        <div class="dados-estrutura">
            <h2 class="titulo">ESTRUTURA</h2>
            <p class="frase">{{ $empresa->frase_estrutura }}</p>
            <p class="onde-estamos">ONDE ESTAMOS?</p>
            <div class="localizacoes">
                <div class="centro-dist">
                    <img src="{{ asset('assets/img/layout/icone-estrutura-centrodistribuicao.svg') }}" alt="" class="img-centro">
                    <p class="nome">Centros de Distribuição</p>
                    @php 
                    $centros = explode("|", $empresa->centros_distribuicao); 
                    $str = ['<', '/', '</', 'p>', '&iacute;', '&atilde;', '&aacute;', '&ocirc;', '&eacute;'];
                    $rep = ['', '', '', '', 'í', 'ã', 'á', 'ô', 'é'];
                    @endphp
                    @foreach($centros as $centro)
                    <p class="centro">{{ str_replace($str, $rep, $centro) }}</p>
                    @endforeach
                </div>
                <div class="cta-laboratorios">
                    <img src="{{ asset('assets/img/layout/icone-estrutura-cta.svg') }}" alt="" class="img-cta">
                    <p class="nome">CTA (Laboratórios)</p>
                    @php 
                    $laboratorios = explode("|", $empresa->laboratorios); 
                    $str = ['<', '/', '</', 'p>', '&iacute;', '&atilde;', '&aacute;', '&ocirc;', '&eacute;'];
                    $rep = ['', '', '', '', 'í', 'ã', 'á', 'ô', 'é'];
                    @endphp
                    @foreach($laboratorios as $laboratorio)
                    <p class="laboratorio">{{ str_replace($str, $rep, $laboratorio) }}</p>
                    @endforeach
                </div>
                <div class="escritorios">
                    <img src="{{ asset('assets/img/layout/icone-estrutura-escritorios.svg') }}" alt="" class="img-escritorio">
                    <p class="nome">Escritórios</p>
                    @php 
                    $escritorios = explode("|", $empresa->escritorios); 
                    $str = ['<', '/', '</', 'p>', '&iacute;', '&atilde;', '&aacute;', '&ocirc;', '&eacute;'];
                    $rep = ['', '', '', '', 'í', 'ã', 'á', 'ô', 'é'];
                    @endphp
                    @foreach($escritorios as $escritorio)
                    <p class="escritorio">{{ str_replace($str, $rep, $escritorio) }}</p>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="mapa">
            <img src="{{ asset('assets/img/empresa/'.$empresa->img_mapa) }}" alt="" class="img-mapa">
        </div>
    </div>

    <div class="qualidade">
        <div class="certificado">
            <h2 class="titulo-qualidade">QUALIDADE</h2>
            <p class="frase-qualidade">{{ $empresa->titulo_certificado }}</p>
            <img src="{{ asset('assets/img/empresa/'.$empresa->img_certificado) }}" alt="" class="img-selo">
        </div>
        <div class="textos-qualidade">
            <h2 class="titulo-politica">POLÍTICA DE QUALIDADE</h2>
            <div class="texto-politica">{!! $empresa->politica_qualidade !!}</div>
            <h2 class="titulo-objetivos">OBJETIVOS DE QUALIDADE</h2>
            <div class="texto-objetivos">{!! $empresa->objetivos_qualidade !!}</div>
        </div>
    </div>

    <div class="servicos">
        <img src="{{ asset('assets/img/empresa/'.$empresa->img_servicos) }}" alt="" class="img-bg-servico">
        <div class="textos-servicos">
            <p class="titulo-servicos">{{ $empresa->titulo_servicos }}</p>
            <p class="frase-servicos">{{ $empresa->frase_servicos }}</p>
            <a href="{{ route('servico-ao-cliente') }}" class="link-servicos">{{ $empresa->btn_servicos }}</a>
        </div>
    </div>

</section>

@endsection