@extends('frontend.layout.template')

@section('content')

<section class="home">

    <div class="banners">
        @foreach($banners as $banner)
        @if($banner->link)
        <a href="{{ $banner->link }}" class="banner">
            <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="img-banner" alt="{{ $banner->titulo }}">
            <div class="overlay-textos">
                <p class="titulo">{{ $banner->titulo }}</p>
                <p class="frase">{{ $banner->frase }}</p>
            </div>
        </a>
        @else
        <div class="banner">
            <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="img-banner" alt="{{ $banner->titulo }}">
            <div class="overlay-textos">
                <p class="titulo">{{ $banner->titulo }}</p>
                <p class="frase">{{ $banner->frase }}</p>
            </div>
        </div>
        @endif
        @endforeach
        <img src="{{ asset('assets/img/layout/setinha-banner-home.svg') }}" alt="" class="cycle-pager">
    </div>

    <div class="produtos">
        <p class="frase-segmentos">{{ $home->frase_segmentos }}</p>
        <div class="linhas">
            @foreach($linhas as $linha)
            <a href="{{ route('produtos.linha', ['linha_slug' => $linha->slug]) }}" class="link-linha">
                <img src="{{ asset('assets/img/produtos/linhas/miniaturas/'.$linha->capa) }}" class="img-linha" alt="{{ $linha->titulo }}">
                <p class="titulo-linha">{{ $linha->titulo }}</p>
            </a>
            @endforeach
        </div>
        <p class="frase-filtro">ENCONTRE O PRODUTO ADEQUADO PARA SEU NEGÓCIO</p>
        <div class="filtro">
            <select name="categoria_id" class="categorias-filtro" id="filtroCategorias">
                <option value="" selected>Por TIPO de Produto</option>
                @foreach($categorias as $categoria)
                <option value="{{ $categoria->slug }}">{{ $categoria->titulo }}</option>
                @endforeach
            </select>
            <select name="aplicacao_id" class="aplicacoes-filtro" id="filtroAplicacoes">
                <option value="" selected>Por APLICAÇÃO no Mercado</option>
                @foreach($aplicacoes as $aplicacao)
                <option value="{{ $aplicacao->slug }}">{{ $aplicacao->titulo }}</option>
                @endforeach
            </select>
            <a href="" class="btn-filtro-buscar" id="btnFiltroBuscar">BUSCAR</a>
        </div>
        <div class="fale-consultores">
            <hr class="linha-btn">
            <a href="{{ route('contato') }}" class="link-fale-consultores">Ou fale diretamente com um de nossos CONSULTORES</a>
        </div>
    </div>

    <div class="servicos">
        <p class="titulo-servicos">{{ $home->titulo_servicos }}</p>
        <p class="frase-servicos">{{ $home->frase_servicos }}</p>
        <div class="itens-servicos">
            @foreach($servicos as $servico)
            <a href="{{ route('servico-ao-cliente.show', $servico->slug) }}" class="link-servico {{$servico->slug}}">
                <div class="icone"></div>
                <div class="textos-servico">
                    <p class="titulo">{{ $servico->titulo }}</p>
                    <p class="subtitulo">{{ $servico->subtitulo }}</p>
                </div>
                <img src="{{ asset('assets/img/layout/seta-fina.svg') }}" alt="" class="img-setinha">
            </a>
            @endforeach
        </div>
    </div>

    <div class="novidades">
        <h2 class="titulo-novidades">COMUNICAÇÃO</h2>
        <p class="frase-novidades">Confira as últimas notícias</p>
        <div class="noticias">
            @foreach($noticias as $noticia)
            <a href="{{ route('noticias.showNoticia', $noticia->slug) }}" class="link-noticia">
                <div class="capa-noticia">
                    <img src="{{ asset('assets/img/noticias/'.$noticia->capa) }}" class="img-capa" alt="{{ $noticia->titulo }}">
                </div>
                <div class="textos-noticia">
                    <p class="titulo-noticia">{{ $noticia->titulo }}</p>
                    <p class="frase-noticia">{{ $noticia->frase }}</p>
                </div>
            </a>
            @endforeach
        </div>
        <a href="{{ route('noticias') }}" class="link-todas-noticias">ver todas as notícias</a>
    </div>

</section>

@endsection