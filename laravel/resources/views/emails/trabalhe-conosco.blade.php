<!DOCTYPE html>
<html>

<head>
    <title>[TRABALHE CONOSCO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Currículo:</span> 
    <a href="{{ route('trabalhe-conosco.arquivo', $contato['id']) }}" style='font-weight:bold;font-size:16px;font-family:Verdana;'>Clique aqui para visualizar o arquivo.</a>
</body>

</html>