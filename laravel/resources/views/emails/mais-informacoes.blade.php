<!DOCTYPE html>
<html>

<head>
    <title>[PRODUTOS - MAIS INFORMAÇÕES] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato['nome'] }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato['email'] }}</span><br>
    @if($contato['telefone'])
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato['telefone'] }}</span><br>
    @endif
    @if($contato['empresa'])
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato['empresa'] }}</span><br>
    @endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato['mensagem'] }}</span>

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Produto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato['produto'] }}</span><br>

</body>

</html>