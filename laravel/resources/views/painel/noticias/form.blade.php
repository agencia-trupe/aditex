@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('data', 'Data') !!}
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/noticias/'.$noticia->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::text('frase', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('texto_ver_mais', 'Texto (VER MAIS)') !!}
    {!! Form::textarea('texto_ver_mais', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($submitText == 'Alterar')
    @if($noticia->arquivo)
    <a href="{{ 'http://www.aditex.com.br/assets/arquivos/'.$noticia->arquivo }}" target="_blank" style="display:block; margin-bottom: 10px; max-width: 100%;">{{ $noticia->arquivo }}</a>
    @endif
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('noticias.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>