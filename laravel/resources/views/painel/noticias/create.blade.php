@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>NOTÍCIAS |</small> Adicionar Notícia</h2>
</legend>

{!! Form::open(['route' => 'noticias.store', 'files' => true]) !!}

@include('painel.noticias.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection