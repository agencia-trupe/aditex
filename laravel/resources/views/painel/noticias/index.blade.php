@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">NOTÍCIAS</h2>
    <a href="{{ route('noticias.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Notícia
    </a>
</legend>

@if(!count($noticias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="noticias">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Capa</th>
                <th scope="col">Titulo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($noticias as $noticia)
            <tr id="{{ $noticia->id }}">
                <td>{{ strftime("%d/%m/%Y", strtotime($noticia->data)) }}</td>
                <td>
                    <img src="{{ asset('assets/img/noticias/'.$noticia->capa) }}" style="width: 100%; max-width:100px;" alt="">
                </td>
                <td>{{ $noticia->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['noticias.destroy', $noticia->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('noticias.edit', $noticia->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection