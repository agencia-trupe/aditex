@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">PRODUTOS - Dados da Página</h2>
</legend>

{!! Form::model($produtosPag, [
'route' => ['produtos-pagina.update', $produtosPag->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.produtos.pagina.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection