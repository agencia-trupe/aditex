@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTOS |</small> Adicionar Produto</h2>
</legend>

{!! Form::open(['route' => 'produtos.store', 'files' => true]) !!}

@include('painel.produtos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection