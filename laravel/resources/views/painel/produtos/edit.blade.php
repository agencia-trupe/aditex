@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTOS |</small> Editar Produto</h2>
</legend>

{!! Form::model($produto, [
'route' => ['produtos.update', $produto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.produtos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection