@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTOS - Linhas/Mercados |</small> Editar Linha</h2>
</legend>

{!! Form::model($linha, [
'route' => ['linhas.update', $linha->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.produtos.linhas.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection