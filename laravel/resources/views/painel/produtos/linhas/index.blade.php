@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PRODUTOS - Linhas/Mercados</h2>
    <a href="{{ route('linhas.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Linha
    </a>
</legend>

@if(!count($linhas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="linhas">
        <thead>
            <tr>
                <th scope="col">Capa</th>
                <th scope="col">Titulo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($linhas as $linha)
            <tr id="{{ $linha->id }}">
                <td>
                    <img src="{{ asset('assets/img/produtos/linhas/miniaturas/'.$linha->capa) }}" style="width: 100%; max-width:150px;" alt="">
                </td>
                <td>{{ $linha->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['linhas.destroy', $linha->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('linhas.edit', $linha->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection