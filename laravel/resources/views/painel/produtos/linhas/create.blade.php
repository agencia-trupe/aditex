@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTOS - Linhas/Mercados |</small> Adicionar Linha</h2>
</legend>

{!! Form::open(['route' => 'linhas.store', 'files' => true]) !!}

@include('painel.produtos.linhas.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection