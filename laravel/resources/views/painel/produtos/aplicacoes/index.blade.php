@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PRODUTOS - Aplicações</h2>
    <a href="{{ route('aplicacoes.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Aplicação
    </a>
</legend>

@if(!count($aplicacoes))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="aplicacoes">
        <thead>
            <tr>
                <th scope="col">Titulo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($aplicacoes as $aplicaco)
            <tr id="{{ $aplicaco->id }}">
                <td>{{ $aplicaco->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['aplicacoes.destroy', $aplicaco->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('aplicacoes.edit', $aplicaco->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection