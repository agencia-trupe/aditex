@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTOS - Aplicações |</small> Editar Aplicação</h2>
</legend>

{!! Form::model($aplicaco, [
'route' => ['aplicacoes.update', $aplicaco->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.produtos.aplicacoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection