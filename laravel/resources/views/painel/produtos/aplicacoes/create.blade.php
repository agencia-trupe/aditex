@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTOS - Aplicações |</small> Adicionar Aplicação</h2>
</legend>

{!! Form::open(['route' => 'aplicacoes.store', 'files' => true]) !!}

@include('painel.produtos.aplicacoes.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection