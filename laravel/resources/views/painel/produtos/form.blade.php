@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('categoria_id', 'Categoria') !!}
    {!! Form::select('categoria_id', $categorias , old('categoria_id'), ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/miniaturas/'.$produto->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('titulo', 'Título') !!}
        {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('subtitulo', 'Subtítulo') !!}
        {!! Form::text('subtitulo', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('embalagem', 'Embalagem') !!}
    {!! Form::textarea('embalagem', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('validade', 'Validade') !!}
    {!! Form::text('validade', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('armazenamento', 'Armazenamento') !!}
    {!! Form::textarea('armazenamento', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('linhas', 'Linhas/Mercados') !!}
    @if(isset($produtosLinhas))
    @foreach($linhas as $linha)
    <label style="padding:0; margin:0; display:block;">
        <input type="checkbox" name="linhas[]" value="{{ $linha->slug }}" @if(in_array($linha['slug'], $produtosLinhas)) checked @endif>
        {{ $linha->titulo }}
    </label>
    @endforeach
    @else
    @foreach($linhas as $linha)
    <label style="padding:0; margin:0; display:block;">
        <input type="checkbox" name="linhas[]" value="{{ $linha->slug }}">
        {{ $linha->titulo }}
    </label>
    @endforeach
    @endif
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('aplicacoes', 'Aplicações') !!}
    @foreach($aplicacoes as $aplicacao)
    <label style="padding:0; margin:0; display:block;">
        @if($produtosAplicacoes == null)
        <input type="checkbox" name="aplicacoes[]" value="{{ $aplicacao->slug }}">
        @else
        <input type="checkbox" name="aplicacoes[]" value="{{ $aplicacao->slug }}" @if(in_array($aplicacao['slug'], $produtosAplicacoes)) checked @endif>
        @endif
        {{ $aplicacao->titulo }}
    </label>
    @endforeach
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('produtos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>