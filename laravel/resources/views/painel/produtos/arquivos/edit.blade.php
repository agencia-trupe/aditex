@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTO: {{ $produto->titulo }} |</small> Editar Arquivo</h2>
</legend>

{!! Form::model($arquivo, [
'route' => ['produtos.arquivos.update', $produto->id, $arquivo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.produtos.arquivos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection