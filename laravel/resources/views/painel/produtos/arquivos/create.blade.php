@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTO: {{ $produto->titulo }} |</small> Adicionar Arquivo</h2>
</legend>

{!! Form::model($produto, [
'route' => ['produtos.arquivos.store', $produto->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.produtos.arquivos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection