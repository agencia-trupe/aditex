@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PRODUTO: {{ $produto->titulo }} - Arquivos</h2>
    <a href="{{ route('produtos.arquivos.create', $produto->id) }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Arquivo
    </a>
</legend>

@if(!count($arquivos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos_arquivos">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Titulo</th>
                <th scope="col">Arquivo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($arquivos as $arquivo)
            <tr id="{{ $arquivo->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>{{ $arquivo->titulo_arquivo }}</td>
                <td>
                    <a href="{{ 'http://www.aditex.com.br/assets/arquivos/produtos/'.$arquivo->arquivo }}" target="_blank">{{ $arquivo->arquivo }}</a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['produtos.arquivos.destroy', $produto->id, $arquivo->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('produtos.arquivos.edit', [$produto->id, $arquivo->id] ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection