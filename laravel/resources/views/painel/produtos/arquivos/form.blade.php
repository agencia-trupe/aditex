@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_arquivo', 'Título do Arquivo') !!}
    {!! Form::text('titulo_arquivo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($submitText == 'Alterar')
    @if($arquivo->arquivo)
    <a href="{{ 'http://www.aditex.com.br/assets/arquivos/produtos/'.$arquivo->arquivo }}" target="_blank" style="display:block; margin-bottom: 10px; max-width: 100%;">{{ $arquivo->arquivo }}</a>
    @endif
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('produtos.arquivos.index', $produto->id) }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>