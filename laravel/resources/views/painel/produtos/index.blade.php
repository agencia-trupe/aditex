@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PRODUTOS</h2>
    <a href="{{ route('produtos.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Produto
    </a>
</legend>

<div class="mt-3 mb-5">
    <h4>Categorias:</h4>
    <div class="row ps-2">
        @if(isset($_GET['categoria']))
        <a href="{{ route('produtos.index') }}" class="btn btn-primary col-12 col-md-3 m-1">Voltar para todos os produtos</a>
        @endif
        @foreach($categorias as $categoria)
        <a href="{{ route('produtos.index', ['categoria' => $categoria->id]) }}" class="btn btn-secondary col-12 col-md-2 m-1 {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) ? 'active' : '' }}">{{ $categoria->titulo }}</a>
        @endforeach
    </div>
</div>

@if(!count($produtos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos">
        <thead>
            <tr>
                <th scope="col">Capa</th>
                <th scope="col">Titulo</th>
                <th scope="col">Subtítulo</th>
                <th scope="col">Gerenciar</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($produtos as $produto)
            <tr id="{{ $produto->id }}">
                <td>
                    <img src="{{ asset('assets/img/produtos/miniaturas/'.$produto->capa) }}" style="width: 100%; max-width:100px;" alt="">
                </td>
                <td>{{ $produto->titulo }}</td>
                <td>{{ $produto->subtitulo }}</td>
                <td>
                    <a href="{{ route('produtos.arquivos.index', $produto->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-file-earmark-text me-2"></i>ARQUIVOS
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['produtos.destroy', $produto->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('produtos.edit', $produto->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection