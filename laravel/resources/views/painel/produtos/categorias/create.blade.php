@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PRODUTOS - Categorias |</small> Adicionar Categoria</h2>
</legend>

{!! Form::open(['route' => 'categorias.store', 'files' => true]) !!}

@include('painel.produtos.categorias.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection