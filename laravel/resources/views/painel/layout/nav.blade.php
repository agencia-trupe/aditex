<ul class="nav navbar-nav">

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['home*', 'banners*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            Home <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('home.index') }}" class="dropdown-item @if(Tools::routeIs('home*')) active @endif">Dados da página</a>
            </li>
            <li>
                <a href="{{ route('banners.index') }}" class="dropdown-item @if(Tools::routeIs('banners*')) active @endif">Banners</a>
            </li>
        </ul>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['empresa*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuEmpresa" data-bs-toggle="dropdown" aria-expanded="false">
            Empresa <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuEmpresa">
            <li>
                <a href="{{ route('empresa.index') }}" class="dropdown-item @if(Tools::routeIs('empresa.index')) active @endif">Dados da página</a>
            </li>
            <li>
                <a href="{{ route('empresa-informacoes.index') }}" class="dropdown-item @if(Tools::routeIs('empresa-informacoes.index')) active @endif">Informações sobre empresa</a>
            </li>
        </ul>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['produtos*', 'linhas*', 'categorias*', 'aplicacoes*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuProdutos" data-bs-toggle="dropdown" aria-expanded="false">
            Produtos <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuProdutos">
            <li>
                <a href="{{ route('produtos-pagina.index') }}" class="dropdown-item @if(Tools::routeIs('produtos-pagina*')) active @endif">Dados da página</a>
            </li>
            <li>
                <a href="{{ route('linhas.index') }}" class="dropdown-item @if(Tools::routeIs('linhas*')) active @endif">Linhas/Mercados</a>
            </li>
            <li>
                <a href="{{ route('categorias.index') }}" class="dropdown-item @if(Tools::routeIs('categorias*')) active @endif">Categorias</a>
            </li>
            <li>
                <a href="{{ route('aplicacoes.index') }}" class="dropdown-item @if(Tools::routeIs('aplicacoes*')) active @endif">Aplicações</a>
            </li>
            <li>
                <a href="{{ route('produtos.index') }}" class="dropdown-item @if(Tools::routeIs('produtos.*')) active @endif">Produtos</a>
            </li>
        </ul>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['servico-ao-cliente*', 'servicos*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuServiços" data-bs-toggle="dropdown" aria-expanded="false">
            Serviço ao Cliente <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuServiços">
            <li>
                <a href="{{ route('servico-ao-cliente.index') }}" class="dropdown-item @if(Tools::routeIs('servico-ao-cliente.index')) active @endif">Dados da página</a>
            </li>
            <li>
                <a href="{{ route('servicos.index') }}" class="dropdown-item @if(Tools::routeIs('servicos*')) active @endif">Serviços</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('noticias.index') }}" class="nav-link px-3 @if(Tools::routeIs('noticias*')) active @endif">Notícias</a>
    </li>

    <li>
        <a href="{{ route('contatos.index') }}" class="nav-link px-3 @if(Tools::routeIs('contatos.index')) active @endif">Contatos Aditex</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['trabalhe-conosco*', 'mais-informacoes*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Contatos Recebidos
            @php $total = $contatosInfosNaoLidos + $contatosCurriculosNaoLidos; @endphp
            @if($total >= 1)
            <span class="label label-success ms-1">{{ $total }}</span>
            @endif
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('trabalhe-conosco.index') }}" class="dropdown-item @if(Tools::routeIs('trabalhe-conosco*')) active @endif d-flex align-items-center">
                    Trabalhe conosco
                    @if($contatosCurriculosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosCurriculosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li>
                <a href="{{ route('mais-informacoes.index') }}" class="dropdown-item @if(Tools::routeIs('mais-informacoes*')) active @endif d-flex align-items-center">
                    Mais Informações de Produtos
                    @if($contatosInfosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosInfosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>