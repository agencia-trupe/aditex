@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/servicos/'.$servico->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<input name="titulo" type="hidden" value="{{ $servico->titulo }}">

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::text('subtitulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('servicos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>