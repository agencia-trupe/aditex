@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>SERVIÇO: {{ $servico->titulo }} |</small> Adicionar Informação</h2>
</legend>

{!! Form::model($servico, [
'route' => ['servicos.informacoes.store', $servico->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.servico-ao-cliente.servicos.informacoes.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection