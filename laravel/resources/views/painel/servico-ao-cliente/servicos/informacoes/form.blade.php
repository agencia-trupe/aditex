@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_info', 'Título') !!}
    {!! Form::text('titulo_info', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_info', 'Frase') !!}
    {!! Form::text('frase_info', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('link', 'Link (opcional)') !!}
    {!! Form::text('link', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('servicos.informacoes.index', $servico->id) }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>