@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>SERVIÇO: {{ $servico->titulo }} |</small> Editar Informação</h2>
</legend>

{!! Form::model($informacao, [
'route' => ['servicos.informacoes.update', $servico->id, $informacao->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.servico-ao-cliente.servicos.informacoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection