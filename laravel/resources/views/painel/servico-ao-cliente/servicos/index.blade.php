@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">SERVIÇOS</h2>
</legend>

@if(!count($servicos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="servicos">
        <thead>
            <tr>
                <th scope="col">Capa</th>
                <th scope="col">Titulo</th>
                <th scope="col">Gerenciar</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($servicos as $servico)
            <tr id="{{ $servico->id }}">
                <td>
                    <img src="{{ asset('assets/img/servicos/'.$servico->capa) }}" style="width: 100%; max-width:100px;" alt="">
                </td>
                <td>{{ $servico->titulo }}</td>
                <td class="d-flex flex-row align-items-center">
                    <a href="{{ route('servicos.imagens.index', $servico->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-file-earmark-image me-2 mb-1"></i>IMAGENS
                    </a>
                    <a href="{{ route('servicos.informacoes.index', $servico->id) }}" class="btn btn-secondary btn-gerenciar btn-sm ms-2">
                        <i class="bi bi-card-list me-2 mb-1"></i>INFORMAÇÕES
                    </a>
                </td>
                <td class="crud-actions">
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('servicos.edit', $servico->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection