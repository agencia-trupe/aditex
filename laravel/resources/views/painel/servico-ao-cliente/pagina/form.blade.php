@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa da Página') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/servico-ao-cliente/'.$servicoAoCliente->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_capa', 'Frase - Capa') !!}
    {!! Form::text('frase_capa', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_capa', 'Título - Capa') !!}
    {!! Form::text('titulo_capa', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_texto', 'Frase (após o texto)') !!}
    {!! Form::text('frase_texto', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('servico-ao-cliente.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>