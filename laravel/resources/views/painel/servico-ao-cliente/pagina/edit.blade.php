@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">SERVIÇO AO CLIENTE - Dados da Página</h2>
</legend>

{!! Form::model($servicoAoCliente, [
'route' => ['servico-ao-cliente.update', $servicoAoCliente->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.servico-ao-cliente.pagina.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection