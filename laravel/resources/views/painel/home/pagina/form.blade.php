@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_segmentos', 'Frase Segmentos (após banners)') !!}
    {!! Form::text('frase_segmentos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_servicos', 'Título Serviços (área sobre serviços)') !!}
    {!! Form::text('titulo_servicos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_servicos', 'Frase Serviços (área sobre serviços)') !!}
    {!! Form::text('frase_servicos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('home.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>