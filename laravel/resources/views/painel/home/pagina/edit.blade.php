@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">HOME - Dados da Página</h2>
</legend>

{!! Form::model($home, [
'route' => ['home.update', $home->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.pagina.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection