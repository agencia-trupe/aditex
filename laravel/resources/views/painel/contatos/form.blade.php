@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa') !!}
    @if($contato->capa)
    <img src="{{ url('assets/img/contatos/'.$contato->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('email_produtos', 'E-mail (recebimentos de formulários de produtos)') !!}
    {!! Form::text('email_produtos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('email_trab_conosco', 'E-mail (recebimentos de trabalhe conosco)') !!}
    {!! Form::text('email_trab_conosco', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('atendimento', 'Dados de atendimento') !!}
    {!! Form::text('atendimento', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('filial_1', 'Filial 1 - Cidade') !!}
        {!! Form::text('filial_1', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('telefone_1', 'Filial 1 - Telefone') !!}
        {!! Form::text('telefone_1', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('filial_2', 'Filial 2 - Cidade') !!}
        {!! Form::text('filial_2', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('telefone_2', 'Filial 2 - Telefone') !!}
        {!! Form::text('telefone_2', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('email_vendas', 'E-mail Vendas') !!}
    {!! Form::text('email_vendas', null, ['class' => 'form-control input-text']) !!}
</div>
<div class="mb-3 col-12">
    {!! Form::label('email_marketing', 'E-mail Marketing') !!}
    {!! Form::text('email_marketing', null, ['class' => 'form-control input-text']) !!}
</div>
<div class="mb-3 col-12">
    {!! Form::label('email_compras', 'E-mail Compras') !!}
    {!! Form::text('email_compras', null, ['class' => 'form-control input-text']) !!}
</div>
<div class="mb-3 col-12">
    {!! Form::label('email_rh', 'E-mail RH') !!}
    {!! Form::text('email_rh', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('whatsapp', 'Whatsapp') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('contatos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>