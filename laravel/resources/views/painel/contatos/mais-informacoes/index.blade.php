@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="mb-4">
    <h2 class="m-0">MAIS INFORMAÇÕES DE PRODUTOS</h2>
</legend>

@if(!count($contatos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="contatos_mais_informacoes">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Produto</th>
                <th scope="col">Comunicados</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($contatos as $contato)
            <tr class="@if(!$contato->lido)alert alert-warning @endif" id="{{ $contato->id }}">
                <td data-order="{{ $contato->created_at_order }}">{{ $contato->created_at }}</td>
                <td>{{ $contato->nome }}</td>
                <td class="d-flex flex-row align-items-center">
                    <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $contato->email }}">
                        <i class="bi bi-clipboard"></i>
                    </button>
                    {{ $contato->email }}
                </td>
                @php
                $produto = $produtos->find($contato->produto_id)->titulo;
                @endphp
                <td>{{ $produto }}</td>
                <td>
                    @if($contato->aceito == 0)
                    <i class="bi bi-x-circle-fill me-1"></i> Não
                    @else 
                    <i class="bi bi-check-circle-fill me-1"></i> Sim
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['mais-informacoes.destroy', $contato->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('mais-informacoes.show', $contato->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-chat-text-fill me-2"></i>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>

                        <a href="{{ route('mais-informacoes.toggle', $contato->id) }}" class="btn btn-sm {{ ($contato->lido ? 'btn-warning' : 'btn-success') }}">
                            @if($contato->lido)
                            <i class="bi bi-arrow-repeat"></i>
                            @else
                            <i class="bi bi-check2-circle"></i>
                            @endif
                        </a>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endif

@endsection