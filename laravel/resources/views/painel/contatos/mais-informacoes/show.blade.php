@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">MAIS INFORMAÇÕES DE PRODUTOS</h2>
</legend>

<div class="mb-3 col-12">
    <label>Data</label>
    <div class="well">{{ $contatoInfo->created_at }}</div>
</div>

<div class="mb-3 col-12">
    <label>Nome</label>
    <div class="well">{{ $contatoInfo->nome }}</div>
</div>

<div class="mb-3 col-12">
    <label>E-mail</label>
    <div class="well">
        <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $contatoInfo->email }}">
            <i class="bi bi-clipboard"></i>
        </button>
        {{ $contatoInfo->email }}
    </div>
</div>

@if($contatoInfo->telefone)
<div class="mb-3 col-12">
    <label>Telefone</label>
    <div class="well">{{ $contatoInfo->telefone }}</div>
</div>
@endif

@if($contatoInfo->empresa)
<div class="mb-3 col-12">
    <label>Telefone</label>
    <div class="well">{{ $contatoInfo->empresa }}</div>
</div>
@endif

<div class="mb-3 col-12">
    <label>Mensagem</label>
    <div class="well-msg">{!! $contatoInfo->mensagem !!}</div>
</div>

<div class="mb-3 col-12">
    <label>Aceita receber comunicados?</label>
    @if($contatoInfo->aceito == 0)
    <i class="bi bi-x-circle-fill mx-1"></i> Não
    @else
    <i class="bi bi-check-circle-fill mx-1"></i> Sim
    @endif
</div>

<div class="mb-3 col-12">
    <label>Produto</label>
    <div class="well-msg">{{ $produto->titulo }}</div>
</div>

<div class="d-flex align-items-center mt-4">
    <a href="{{ route('mais-informacoes.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>

@stop