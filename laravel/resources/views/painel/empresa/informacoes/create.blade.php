@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>EMPRESA - Informações |</small> Adicionar Item</h2>
</legend>

{!! Form::open(['route' => 'empresa-informacoes.store', 'files' => true]) !!}

@include('painel.empresa.informacoes.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection