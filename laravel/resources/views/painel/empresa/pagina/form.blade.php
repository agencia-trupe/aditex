@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa da Página') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresa/'.$empresa->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('texto_historia', 'Texto - História') !!}
    {!! Form::textarea('texto_historia', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_estrutura', 'Frase - Estrutura') !!}
    {!! Form::text('frase_estrutura', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('centros_distribuicao', 'Centros de Distribuição') !!}
        {!! Form::textarea('centros_distribuicao', null, ['class' => 'form-control input-textarea']) !!}
        <p class="obs"><strong>Exemplo:</strong> Item 1|Item 2|Item 3 (com | entre os itens, <strong>SEM ESPAÇOS</strong>)</p>
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('laboratorios', 'CTA (Laboratórios)') !!}
        {!! Form::textarea('laboratorios', null, ['class' => 'form-control input-textarea']) !!}
        <p class="obs"><strong>Exemplo:</strong> Item 1|Item 2|Item 3 (com | entre os itens, <strong>SEM ESPAÇOS</strong>)</p>
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('escritorios', 'Escritórios') !!}
        {!! Form::textarea('escritorios', null, ['class' => 'form-control input-textarea']) !!}
        <p class="obs"><strong>Exemplo:</strong> Item 1|Item 2|Item 3 (com | entre os itens, <strong>SEM ESPAÇOS</strong>)</p>
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('img_mapa', 'Mapa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresa/'.$empresa->img_mapa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('img_mapa', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_certificado', 'Título Qualidade/Certificado') !!}
    {!! Form::text('titulo_certificado', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('img_certificado', 'Imagem Certificado') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresa/'.$empresa->img_certificado) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('img_certificado', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('politica_qualidade', 'Política de Qualidade') !!}
    {!! Form::textarea('politica_qualidade', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('objetivos_qualidade', 'Objetivos de Qualidade') !!}
    {!! Form::textarea('objetivos_qualidade', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('img_servicos', 'Imagem Serviços (área sobre serviços)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresa/'.$empresa->img_servicos) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('img_servicos', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('titulo_servicos', 'Título Serviços (área sobre serviços)') !!}
    {!! Form::text('titulo_servicos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('frase_servicos', 'Frase Serviços (área sobre serviços)') !!}
    {!! Form::text('frase_servicos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('btn_servicos', 'Botão Serviços (área sobre serviços)') !!}
    {!! Form::text('btn_servicos', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('home.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>