@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">EMPRESA - Dados da Página</h2>
</legend>

{!! Form::model($empresa, [
'route' => ['empresa.update', $empresa->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.pagina.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection