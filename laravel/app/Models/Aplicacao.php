<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aplicacao extends Model
{
    use HasFactory;

    protected $table = 'aplicacoes';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%');
    }
}
