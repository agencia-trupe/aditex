<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    use HasFactory;

    protected $table = 'noticias';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($data)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 400,
                'height' => null,
                'path'   => 'assets/img/noticias/'
            ],
            [
                'width'  => 1000,
                'height' => null,
                'path'   => 'assets/img/noticias/capas/'
            ]
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto', 'LIKE', '%' . $termo . '%');
    }
}
