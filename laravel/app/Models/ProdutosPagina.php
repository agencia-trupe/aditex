<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdutosPagina extends Model
{
    use HasFactory;

    protected $table = 'produtos_pagina';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/produtos/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo_capa', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase', 'LIKE', '%' . $termo . '%');
    }
}
