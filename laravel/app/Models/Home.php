<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    protected $table = 'home';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('frase_segmentos', 'LIKE', '%' . $termo . '%')
            ->orWhere('titulo_servicos', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase_servicos', 'LIKE', '%' . $termo . '%');
    }
}
