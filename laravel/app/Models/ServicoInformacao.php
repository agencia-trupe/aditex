<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicoInformacao extends Model
{
    use HasFactory;

    protected $table = 'servicos_informacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeServico($query, $id)
    {
        return $query->where('servico_id', $id);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo_info', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase_info', 'LIKE', '%' . $termo . '%');
    }
}
