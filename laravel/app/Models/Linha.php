<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{
    use HasFactory;

    protected $table = 'linhas';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 1000,
                'height' => null,
                'path'   => 'assets/img/produtos/linhas/'
            ],
            [
                'width'  => 200,
                'height' => null,
                'path'   => 'assets/img/produtos/linhas/miniaturas/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/produtos/linhas/originais/'
            ]
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase', 'LIKE', '%' . $termo . '%');
    }
}
