<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdutoArquivo extends Model
{
    use HasFactory;

    protected $table = 'produtos_arquivos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProdutos($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo_arquivo', 'LIKE', '%' . $termo . '%')
            ->orWhere('arquivo', 'LIKE', '%' . $termo . '%');
    }
}
