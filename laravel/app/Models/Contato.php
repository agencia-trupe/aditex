<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Contato extends Model
{
    use HasFactory;
    use Notifiable;

    protected $table = 'contatos';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 1000,
            'height' => null,
            'path'   => 'assets/img/contatos/'
        ]);
    }
}
