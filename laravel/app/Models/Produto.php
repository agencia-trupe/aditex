<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;

    protected $table = 'produtos';

    protected $guarded = ['id'];

    protected $casts = [
        'aplicacoes' => 'array'
    ];

    public function scopeCategorias($query, $id)
    {
        return $query->where('categoria_id', $id);
    }

    public function especificacoes()
    {
        return $this->hasMany('App\Models\ProdutoEspecificacao', 'produto_id')->ordenados();
    }

    public function arquivos()
    {
        return $this->hasMany('App\Models\ProdutoArquivo', 'produto_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 1000,
                'height' => null,
                'path'   => 'assets/img/produtos/'
            ],
            [
                'width'  => 400,
                'height' => null,
                'path'   => 'assets/img/produtos/miniaturas/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/produtos/originais/'
            ]
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('subtitulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('descricao', 'LIKE', '%' . $termo . '%')
            ->orWhere('embalagem', 'LIKE', '%' . $termo . '%')
            ->orWhere('armazenamento', 'LIKE', '%' . $termo . '%');
    }
}
