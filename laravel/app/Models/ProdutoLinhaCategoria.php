<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdutoLinhaCategoria extends Model
{
    use HasFactory;

    protected $table = 'produtos_linhas_categorias';

    protected $guarded = ['id'];
}
