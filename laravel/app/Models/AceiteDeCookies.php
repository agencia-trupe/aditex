<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AceiteDeCookies extends Model
{
    use HasFactory;
    
    protected $table = 'aceite_de_cookies';

    protected $guarded = ['id'];
}
