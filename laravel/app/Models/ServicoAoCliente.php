<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicoAoCliente extends Model
{
    use HasFactory;

    protected $table = 'servico_ao_cliente';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/servico-ao-cliente/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('frase_capa', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase_texto', 'LIKE', '%' . $termo . '%');
    }
}
