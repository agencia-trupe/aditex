<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PoliticaDePrivacidade extends Model
{
    use HasFactory;

    protected $table = 'politica_de_privacidade';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%' . $termo . '%');
    }
}
