<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicoImagem extends Model
{
    use HasFactory;

    protected $table = 'servicos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeServico($query, $id)
    {
        return $query->where('servico_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 400,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/servicos/imagens/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'    => 'assets/img/servicos/imagens/originais/'
            ]
        ]);
    }
}
