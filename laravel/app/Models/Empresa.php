<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_mapa()
    {
        return CropImage::make('img_mapa', [
            'width'  => 500,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_certificado()
    {
        return CropImage::make('img_certificado', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_img_servicos()
    {
        return CropImage::make('img_servicos', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto_historia', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase_estrutura', 'LIKE', '%' . $termo . '%')
            ->orWhere('politica_qualidade', 'LIKE', '%' . $termo . '%')
            ->orWhere('objetivos_qualidade', 'LIKE', '%' . $termo . '%')
            ->orWhere('titulo_servicos', 'LIKE', '%' . $termo . '%')
            ->orWhere('frase_servicos', 'LIKE', '%' . $termo . '%');
    }
}
