<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'capa'                 => 'required|image',
            'texto_historia'       => 'required',
            'frase_estrutura'      => 'required',
            'centros_distribuicao' => 'required',
            'laboratorios'         => 'required',
            'escritorios'          => 'required',
            'img_mapa'             => 'required|image',
            'titulo_certificado'   => 'required',
            'img_certificado'      => 'required|image',
            'politica_qualidade'   => 'required',
            'objetivos_qualidade'  => 'required',
            'img_servicos'         => 'required|image',
            'titulo_servicos'      => 'required',
            'frase_servicos'       => 'required',
            'btn_servicos'         => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['img_mapa'] = 'image';
            $rules['img_certificado'] = 'image';
            $rules['img_servicos'] = 'image';
        }

        return $rules;
    }
}
