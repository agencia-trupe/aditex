<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoticiasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'data'      => 'required',
            'titulo'    => 'required',
            'frase'     => 'required',
            'texto'     => 'required',
            'capa'      => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa']   = 'image';
        }

        return $rules;
    }
}
