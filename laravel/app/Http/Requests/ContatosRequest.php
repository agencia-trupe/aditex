<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'capa'            => 'required|image',
            'nome'            => 'required',
            'email_produtos'  => 'required|email',
            'email_trab_conosco' => 'required|email',
            'atendimento'     => 'required',
            'filial_1'        => 'required',
            'telefone_1'      => 'required',
            'filial_2'        => 'required',
            'telefone_2'      => 'required',
            'email_vendas'    => 'required|email',
            'email_marketing' => 'required|email',
            'email_compras'   => 'required|email',
            'email_rh'        => 'required|email',
            'whatsapp'        => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
