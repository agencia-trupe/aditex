<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'capa'          => 'required|image',
            'titulo'        => 'required',
            'subtitulo'     => 'required',
            'descricao'     => 'required',
            'embalagem'     => '',
            'armazenamento' => '',
            'validade'      => '',
            'aplicacoes'    => '',
            'linhas'        => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
