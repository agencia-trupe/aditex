<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Aplicacao;
use App\Models\Banner;
use App\Models\Categoria;
use App\Models\Home;
use App\Models\Linha;
use App\Models\Noticia;
use App\Models\Produto;
use App\Models\Servico;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public static function getDolar($data)
    {
        $api = file_get_contents('https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)?%40dataCotacao=%27' . $data . '%27&%24format=json');
        $result = json_decode($api);
        return $result;
    }

    public function index()
    {
        $banners = Banner::ordenados()->get();
        $home = Home::first();
        $linhas = Linha::orderBy('titulo', 'asc')->get();
        $categorias = Categoria::orderBy('titulo', 'asc')->select('slug', 'titulo')->distinct()->get();
        $aplicacoes = Aplicacao::orderBy('titulo', 'asc')->get();
        $servicos = Servico::orderBy('id', 'asc')->get();
        $noticias = Noticia::orderBy('data', 'desc')->take(4)->get();

        return view('frontend.home', compact('banners', 'home', 'linhas', 'categorias', 'aplicacoes', 'servicos', 'noticias'));
    }

    public function getItensFiltro($categoria_slug)
    {
        if ($categoria_slug != "0") {
            $categoria = Categoria::where('slug', $categoria_slug)->first();

            $produtos = Produto::where('categoria_id', $categoria->id)->get();
            $produtos_aplicacoes = [];
            foreach ($produtos as $produto) {
                $produtos_aplicacoes[] = json_decode($produto->aplicacoes);
            }

            $aplicacoes = [];
            foreach ($produtos_aplicacoes as $aplics) {
                foreach ($aplics as $aplic) {
                    $aplicacao = Aplicacao::where('slug', $aplic)->first();
                    if (!in_array($aplicacao, $aplicacoes)) {
                        if (isset($aplicacao)) {
                            $aplicacoes[] = $aplicacao;
                        }
                    }
                }
            }
        }

        return response()->json(['categoria' => $categoria, 'aplicacoes' => $aplicacoes]);
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
