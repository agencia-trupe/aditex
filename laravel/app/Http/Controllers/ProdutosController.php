<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosMaisInformacoesRequest;
use App\Mail\MaisInformacoes;
use App\Models\Aplicacao;
use App\Models\Categoria;
use App\Models\Contato;
use App\Models\ContatoMaisInformacao;
use App\Models\Linha;
use App\Models\Produto;
use App\Models\ProdutoArquivo;
use App\Models\ProdutosPagina;
use App\Notifications\MaisInformacoesNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class ProdutosController extends Controller
{
    public function index()
    {
        $pagina = ProdutosPagina::first();
        $linhas = Linha::orderBy('titulo', 'asc')->get();
        $categorias = Categoria::join('produtos_linhas_categorias', 'produtos_linhas_categorias.categoria_id', '=', 'categorias.id')
            ->select('produtos_linhas_categorias.linha_id', 'categorias.*')
            ->distinct()
            ->orderBy('titulo', 'asc')->get();
        $produtos = Produto::orderBy('titulo', 'asc')->get();

        return view('frontend.produtos', compact('pagina', 'linhas', 'categorias'));
    }

    public function indexLinhas($linha_slug)
    {
        $linha = Linha::where('slug', $linha_slug)->first();
        $categorias = Categoria::join('produtos_linhas_categorias', 'produtos_linhas_categorias.categoria_id', '=', 'categorias.id')
            ->where('produtos_linhas_categorias.linha_id', $linha->id)
            ->select('categorias.*')
            ->distinct()->orderBy('titulo', 'asc')->get();
        $produtos = Produto::join('produtos_linhas_categorias', 'produtos_linhas_categorias.produto_id', '=', 'produtos.id')
            ->where('produtos_linhas_categorias.linha_id', $linha->id)
            ->select('produtos.*')
            ->orderBy('titulo', 'asc')->get();

        return view('frontend.produtos-linha', compact('linha', 'categorias', 'produtos'));
    }

    public function indexLinhasCategorias($linha_slug, $categoria_slug)
    {
        $linha = Linha::where('slug', $linha_slug)->first();
        $categoria = Categoria::where('slug', $categoria_slug)->first();

        $produtos = Produto::join('produtos_linhas_categorias', 'produtos_linhas_categorias.produto_id', '=', 'produtos.id')
            ->where('produtos_linhas_categorias.linha_id', $linha->id)
            ->where('produtos_linhas_categorias.categoria_id', $categoria->id)
            ->select('produtos.*')
            ->orderBy('titulo', 'asc')->get();

        return view('frontend.produtos-linha', compact('linha', 'categoria', 'produtos'));
    }

    public function show($produto_slug)
    {
        $produto = Produto::where('slug', $produto_slug)->first();
        $categoria = Categoria::where('id', $produto->categoria_id)->first();
        $linhas = Linha::join('produtos_linhas_categorias', 'produtos_linhas_categorias.linha_id', '=', 'linhas.id')
            ->where('produtos_linhas_categorias.produto_id', $produto->id)
            ->where('produtos_linhas_categorias.categoria_id', $categoria->id)
            ->get();

        $arquivos = ProdutoArquivo::where('produto_id', $produto->id)->ordenados()->get();

        $produtos_aplicacoes = json_decode($produto->aplicacoes);
        $aplicacoes = [];
        if (isset($produtos_aplicacoes)) {
            foreach ($produtos_aplicacoes as $aplic) {
                $aplicacoes[] = Aplicacao::where('slug', $aplic)->first();
            }
        }
        // dd($categoria);

        return view('frontend.produtos-show', compact('linhas', 'categoria', 'produto', 'arquivos', 'aplicacoes'));
    }

    // public function openArquivoPDF($produto_id, $arquivo_slug)
    // {
    //     $arquivo = ProdutoArquivo::where('slug', $arquivo_slug)->where('produto_id', $produto_id)->select('arquivo')->first();

    //     $arquivoPath = public_path() . "/assets/arquivos/produtos/" . $arquivo->arquivo;

    //     return response()->file($arquivoPath);
    // }

    public function postMaisInformacoes(ContatosMaisInformacoesRequest $request, Produto $produto)
    {
        $data = $request->all();
        $data['produto_id'] = $produto->id;
        if (isset($request->aceito)) {
            $data['aceito'] = $request->aceito;
        } else {
            $data['aceito'] = 0;
        }

        $contato = ContatoMaisInformacao::create($data);

        $contato['produto'] = Produto::where('id', $contato->produto_id)->first()->titulo;

        if (!$email = Contato::first()->email_produtos) {
            return false;
        }

        Mail::to($email)->send(new MaisInformacoes($contato));

        return redirect()->back()->with('enviado', true);
    }

    public function filtroProdutos($categoria_slug, $aplicacao_slug)
    {
        if ($categoria_slug != "0" && $aplicacao_slug == "0") {
            $categoria = Categoria::where('slug', $categoria_slug)->first();
            $produtos = Produto::where('categoria_id', $categoria->id)->orderBy('titulo', 'asc')->get();
            $categorias = null;
        } elseif ($categoria_slug == "0" && $aplicacao_slug != "0") {
            $produtosGeral = Produto::orderBy('titulo', 'asc')->get();
            $produtos = [];
            foreach ($produtosGeral as $produto) {
                $aplicProd = json_decode($produto->aplicacoes);
                if (is_array($aplicProd) && in_array($aplicacao_slug, $aplicProd)) {
                    $produtos[] = $produto;
                }
            }
            $categorias = [];
            foreach ($produtos as $produto) {
                $categorias[] = Categoria::where('id', $produto->categoria_id)->orderBy('titulo', 'asc')->first();
            }
            $categorias = array_unique($categorias);
            $categoria = null;
        } elseif ($categoria_slug != "0" && $aplicacao_slug != "0") {
            $categoria = Categoria::where('slug', $categoria_slug)->first();
            $produtosCat = Produto::where('categoria_id', $categoria->id)->orderBy('titulo', 'asc')->get();
            $produtos = [];
            foreach ($produtosCat as $produto) {
                $aplicProd = json_decode($produto->aplicacoes);
                if (is_array($aplicProd) && in_array($aplicacao_slug, $aplicProd)) {
                    $produtos[] = $produto;
                }
            }
            $categorias = null;
        } else {
            $categoria = Categoria::where('slug', $categoria_slug)->first();
            $categorias = Categoria::orderBy('titulo', 'asc')->get();
            $produtos = Produto::orderBy('titulo', 'asc')->get();
        }

        $linhas = Linha::join('produtos_linhas_categorias', 'produtos_linhas_categorias.linha_id', '=', 'linhas.id')
            ->select('produtos_linhas_categorias.produto_id', 'produtos_linhas_categorias.categoria_id', 'linhas.*')
            ->orderBy('titulo', 'asc')->get();

        return view('frontend.produtos-filtro', compact('categoria', 'produtos', 'linhas', 'categorias'));
    }
}
