<?php

namespace App\Http\Controllers;

use App\Models\Noticia;
use Illuminate\Http\Request;

class NoticiasController extends Controller
{
    public function index()
    {
        $destaque = Noticia::orderBy('data', 'desc')->first();
        $noticias = Noticia::orderBy('data', 'desc')->skip(1)->take(PHP_INT_MAX)->get();

        return view('frontend.noticias', compact('destaque', 'noticias'));
    }

    public function show($noticia_slug)
    {
        $noticiaShow = Noticia::where('slug', $noticia_slug)->first();
        $noticias = Noticia::where('slug', '!=', $noticia_slug)->orderBy('data', 'desc')->take(3)->get();

        return view('frontend.noticias-show', compact('noticiaShow', 'noticias'));
    }

    // public function openPdf($noticia_slug)
    // {
    //     $arquivo = Noticia::where('slug', $noticia_slug)->select('arquivo')->first();

    //     $arquivoPath = public_path() . "/assets/arquivos/" . $arquivo->arquivo;

    //     return response()->file($arquivoPath);
    // }
}
