<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoticiasRequest;
use App\Models\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NoticiasController extends Controller
{
    private function getName($file)
    {
        $extension = $file->getClientOriginalExtension();
        $name  = Str::slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $name .= '_' . date('YmdHis');
        $name .= '.' . $extension;

        return $name;
    }

    public function index()
    {
        $noticias = Noticia::orderBy('data', 'desc')->get();

        return view('painel.noticias.index', compact('noticias'));
    }

    public function create()
    {
        return view('painel.noticias.create');
    }

    public function store(NoticiasRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            if (isset($input['capa'])) $input['capa'] = Noticia::upload_capa();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = $this->getName($file);
                    $path = public_path() . '/assets/arquivos';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            Noticia::create($input);

            return redirect()->route('noticias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Noticia $noticia)
    {
        return view('painel.noticias.edit', compact('noticia'));
    }

    public function update(NoticiasRequest $request, Noticia $noticia)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            if (isset($input['capa'])) $input['capa'] = Noticia::upload_capa();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = $this->getName($file);
                    $path = public_path() . '/assets/arquivos';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $noticia->update($input);

            return redirect()->route('noticias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Noticia $noticia)
    {
        try {
            $noticia->delete();

            return redirect()->route('noticias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
