<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriasRequest;
use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoriasController extends Controller
{
    public function index()
    {
        $categorias = Categoria::orderBy('titulo', 'asc')->get();

        return view('painel.produtos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.produtos.categorias.create');
    }

    public function store(CategoriasRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            Categoria::create($input);

            return redirect()->route('categorias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Categoria $categoria)
    {
        return view('painel.produtos.categorias.edit', compact('categoria'));
    }

    public function update(CategoriasRequest $request, Categoria $categoria)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            $categoria->update($input);

            return redirect()->route('categorias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Categoria $categoria)
    {
        try {
            $categoria->delete();

            return redirect()->route('categorias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
