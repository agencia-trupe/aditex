<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosArquivosRequest;
use App\Models\Produto;
use App\Models\ProdutoArquivo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProdutosArquivosController extends Controller
{
    private function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'Ñ' => 'N'
        );

        return $conversao;
    }

    public function index(Produto $produto)
    {
        $arquivos = ProdutoArquivo::produtos($produto->id)->ordenados()->get();

        return view('painel.produtos.arquivos.index', compact('arquivos', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.produtos.arquivos.create', compact('produto'));
    }

    public function store(Request $request, Produto $produto)
    {
        try {
            $input = $request->all();
            $input['produto_id'] = $produto->id;
            $input['slug'] = Str::slug($request->titulo_arquivo, "-");

            $file = $request->file('arquivo');

            if (!is_null($file)) {
                $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                $path = public_path() . '/assets/arquivos/produtos';
                $file->move($path, $filename);
                $input['arquivo'] = $filename;
            }

            ProdutoArquivo::create($input);

            return redirect()->route('produtos.arquivos.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Produto $produto, ProdutoArquivo $arquivo)
    {
        return view('painel.produtos.arquivos.edit', compact('arquivo', 'produto'));
    }

    public function update(Request $request, Produto $produto, ProdutoArquivo $arquivo)
    {
        try {
            $input = $request->all();
            $input['produto_id'] = $produto->id;
            $input['slug'] = Str::slug($request->titulo_arquivo, "-");

            $file = $request->file('arquivo');

            if (!is_null($file)) {
                $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                $path = public_path() . '/assets/arquivos/produtos';
                $file->move($path, $filename);
                $input['arquivo'] = $filename;
            }

            $arquivo->update($input);

            return redirect()->route('produtos.arquivos.index', $produto->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Produto $produto, ProdutoArquivo $arquivo)
    {
        try {
            $arquivo->delete();

            return redirect()->route('produtos.arquivos.index', $produto->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
