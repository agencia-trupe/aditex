<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\ContatoMaisInformacao;
use App\Models\Produto;
use Illuminate\Http\Request;

class ContatosMaisInformacoesController extends Controller
{
    public function index()
    {
        $contatos = ContatoMaisInformacao::orderBy('created_at', 'DESC')->get();
        $produtos = Produto::all();

        return view('painel.contatos.mais-informacoes.index', compact('contatos', 'produtos'));
    }

    public function show($id)
    {
        $contatoInfo = ContatoMaisInformacao::where('id', $id)->first();
        $contatoInfo->update(['lido' => 1]);

        $produto = Produto::where('id', $contatoInfo->produto_id)->first();

        return view('painel.contatos.mais-informacoes.show', compact('contatoInfo', 'produto'));
    }

    public function destroy($id)
    {
        try {
            $contatoInfo = ContatoMaisInformacao::where('id', $id)->first();
            $contatoInfo->delete();

            return redirect()->route('mais-informacoes.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($id)
    {
        try {
            $contatoInfo = ContatoMaisInformacao::where('id', $id)->first();
            $contatoInfo->update([
                'lido' => !$contatoInfo->lido
            ]);

            return redirect()->route('mais-informacoes.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
