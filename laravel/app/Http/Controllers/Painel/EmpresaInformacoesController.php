<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaInformacoesRequest;
use App\Models\EmpresaInformacao;
use Illuminate\Http\Request;

class EmpresaInformacoesController extends Controller
{
    public function index()
    {
        $informacoes = EmpresaInformacao::ordenados()->get();

        return view('painel.empresa.informacoes.index', compact('informacoes'));
    }

    public function create()
    {
        return view('painel.empresa.informacoes.create');
    }

    public function store(EmpresaInformacoesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EmpresaInformacao::upload_imagem();

            EmpresaInformacao::create($input);

            return redirect()->route('empresa-informacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(EmpresaInformacao $empresa_informaco)
    {
        $empresaInfo = $empresa_informaco;

        return view('painel.empresa.informacoes.edit', compact('empresaInfo'));
    }

    public function update(EmpresaInformacoesRequest $request, EmpresaInformacao $empresa_informaco)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EmpresaInformacao::upload_imagem();

            $empresa_informaco->update($input);

            return redirect()->route('empresa-informacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(EmpresaInformacao $empresa_informaco)
    {
        try {
            $empresa_informaco->delete();

            return redirect()->route('empresa-informacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
