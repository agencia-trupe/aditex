<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AplicacoesRequest;
use App\Models\Aplicacao;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AplicacoesController extends Controller
{
    public function index()
    {
        $aplicacoes = Aplicacao::orderBy('titulo', 'asc')->get();

        return view('painel.produtos.aplicacoes.index', compact('aplicacoes'));
    }

    public function create()
    {
        return view('painel.produtos.aplicacoes.create');
    }

    public function store(AplicacoesRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            Aplicacao::create($input);

            return redirect()->route('aplicacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Aplicacao $aplicaco)
    {
        return view('painel.produtos.aplicacoes.edit', compact('aplicaco'));
    }

    public function update(AplicacoesRequest $request, Aplicacao $aplicaco)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            $aplicaco->update($input);

            return redirect()->route('aplicacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Aplicacao $aplicaco)
    {
        try {
            $aplicaco->delete();

            return redirect()->route('aplicacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
