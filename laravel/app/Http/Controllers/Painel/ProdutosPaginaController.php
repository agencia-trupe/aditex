<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosPaginaRequest;
use App\Models\ProdutosPagina;
use Illuminate\Http\Request;

class ProdutosPaginaController extends Controller
{
    public function index()
    {
        $produtosPag = ProdutosPagina::first();

        return view('painel.produtos.pagina.edit', compact('produtosPag'));
    }

    public function update(ProdutosPaginaRequest $request, ProdutosPagina $produtos_pagina)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ProdutosPagina::upload_capa();

            $produtos_pagina->update($input);

            return redirect()->route('produtos-pagina.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
