<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaRequest;
use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();

        return view('painel.empresa.pagina.edit', compact('empresa'));
    }

    public function update(EmpresaRequest $request, Empresa $empresa)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Empresa::upload_capa();
            if (isset($input['img_mapa'])) $input['img_mapa'] = Empresa::upload_mapa();
            if (isset($input['img_certificado'])) $input['img_certificado'] = Empresa::upload_certificado();
            if (isset($input['img_servicos'])) $input['img_servicos'] = Empresa::upload_img_servicos();

            $empresa->update($input);

            return redirect()->route('empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
