<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosImagensRequest;
use App\Models\Servico;
use App\Models\ServicoImagem;
use Illuminate\Http\Request;

class ServicosImagensController extends Controller
{
    public function index(Servico $servico)
    {
        $imagens = ServicoImagem::servico($servico->id)->ordenados()->get();

        return view('painel.servico-ao-cliente.servicos.imagens.index', compact('imagens', 'servico'));
    }

    public function show(Servico $servico, ServicoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Servico $servico, ServicosImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ServicoImagem::uploadImagem();
            $input['servico_id'] = $servico->id;

            $imagem = ServicoImagem::create($input);

            $view = view('painel.servico-ao-cliente.servicos.imagens.imagem', compact('servico', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Servico $servico, ServicoImagem $imagen)
    {
        try {
            $imagen->delete();

            return redirect()->route('servicos.imagens.index', $servico)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Servico $servico)
    {
        try {
            $servico->imagens()->delete();

            return redirect()->route('servicos.imagens.index', $servico->id)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
