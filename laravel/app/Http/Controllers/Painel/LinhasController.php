<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\LinhasRequest;
use App\Models\Linha;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LinhasController extends Controller
{
    public function index()
    {
        $linhas = Linha::orderBy('titulo', 'asc')->get();

        return view('painel.produtos.linhas.index', compact('linhas'));
    }

    public function create()
    {
        return view('painel.produtos.linhas.create');
    }

    public function store(LinhasRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            if (isset($input['capa'])) $input['capa'] = Linha::upload_capa();

            Linha::create($input);

            return redirect()->route('linhas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Linha $linha)
    {
        return view('painel.produtos.linhas.edit', compact('linha'));
    }

    public function update(LinhasRequest $request, Linha $linha)
    {
        try {
            $input = $request->all();
            $input['slug'] = Str::slug($request->titulo, "-");

            if (isset($input['capa'])) $input['capa'] = Linha::upload_capa();

            $linha->update($input);

            return redirect()->route('linhas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Linha $linha)
    {
        try {
            $linha->delete();

            return redirect()->route('linhas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
