<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatosRequest;
use App\Models\Contato;
use Illuminate\Http\Request;

class ContatosController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('painel.contatos.edit', compact('contato'));
    }

    public function update(ContatosRequest $request, Contato $contato)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Contato::upload_capa();

            $contato->update($input);

            return redirect()->route('contatos.index')->with('success', 'Informações alteradas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar informações: ' . $e->getMessage()]);
        }
    }
}
