<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\TrabalheConosco;
use Illuminate\Http\Request;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        $curriculos = TrabalheConosco::orderBy('created_at', 'DESC')->get();

        return view('painel.contatos.trabalhe-conosco.index', compact('curriculos'));
    }

    public function destroy($id)
    {
        try {
            $trabalheConosco = TrabalheConosco::where('id', $id)->first();
            $trabalheConosco->delete();

            return redirect()->route('trabalhe-conosco.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($id)
    {
        try {
            $trabalheConosco = TrabalheConosco::where('id', $id)->first();
            $trabalheConosco->update([
                'lido' => !$trabalheConosco->lido
            ]);

            return redirect()->route('trabalhe-conosco.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
