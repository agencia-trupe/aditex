<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosRequest;
use App\Models\Aplicacao;
use App\Models\Categoria;
use App\Models\Linha;
use App\Models\Produto;
use App\Models\ProdutoLinhaCategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProdutosController extends Controller
{
    public function index()
    {
        $categorias = Categoria::orderBy('titulo', 'asc')->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $produtos = Produto::where('categoria_id', $categoria)->orderBy('titulo', 'asc')->get();
        } else {
            $produtos = Produto::orderBy('titulo', 'asc')->get();
        }

        return view('painel.produtos.index', compact('produtos', 'categorias'));
    }

    public function create()
    {
        $aplicacoes = Aplicacao::orderBy('titulo', 'asc')->get();
        $linhas = Linha::orderBy('titulo', 'asc')->get();
        $categorias = Categoria::orderBy('titulo', 'asc')->orderBy('titulo', 'asc')->pluck('titulo', 'id');

        $produtosAplicacoes = [];
        $produtosLinhas = [];

        return view('painel.produtos.create', compact('aplicacoes', 'categorias', 'linhas', 'produtosAplicacoes', 'produtosLinhas'));
    }

    public function store(ProdutosRequest $request)
    {
        try {
            $input = $request->all();
            if (isset($input['aplicacoes'])) {} $input['aplicacoes'] = json_encode($request->aplicacoes);
            if (isset($input['linhas'])) $input['linhas'] = json_encode($request->linhas);
            $input['slug'] = Str::slug($request->titulo, "-");

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();

            $produto = Produto::create($input);

            $linhas = $request->linhas;
            $categoria = $request->categoria_id;

            foreach ($linhas as $linha) {
                $linhaP = Linha::where('slug', $linha)->first();
                $data = [
                    'produto_id' => $produto->id,
                    'linha_id' => $linhaP->id,
                    'categoria_id' => $categoria
                ];

                $ligacao = ProdutoLinhaCategoria::where('produto_id', $produto->id)->where('linha_id', $linhaP->id)->where('categoria_id', $categoria)->first();
                if ($ligacao == null) {
                    ProdutoLinhaCategoria::create($data);
                }
            }

            return redirect()->route('produtos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Produto $produto)
    {
        $aplicacoes = Aplicacao::orderBy('titulo', 'asc')->get();
        $linhas = Linha::orderBy('titulo', 'asc')->get();
        $categorias = Categoria::orderBy('titulo', 'asc')->orderBy('titulo', 'asc')->pluck('titulo', 'id');

        $produtosAplicacoes = json_decode($produto->aplicacoes);
        $produtosLinhas = json_decode($produto->linhas);

        return view('painel.produtos.edit', compact('produto', 'aplicacoes', 'linhas', 'produtosAplicacoes', 'categorias', 'produtosLinhas'));
    }

    public function update(ProdutosRequest $request, Produto $produto)
    {
        try {
            $input = $request->all();
            if (isset($input['aplicacoes'])) {} $input['aplicacoes'] = json_encode($request->aplicacoes);
            if (isset($input['linhas'])) $input['linhas'] = json_encode($request->linhas);
            $input['slug'] = Str::slug($request->titulo, "-");

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();

            $produto->update($input);

            $linhas = $request->linhas;
            $categoria = $request->categoria_id;

            $ligacao = ProdutoLinhaCategoria::where('produto_id', $produto->id)->delete();

            foreach ($linhas as $linha) {
                $linhaP = Linha::where('slug', $linha)->first();
                $data = [
                    'produto_id' => $produto->id,
                    'linha_id' => $linhaP->id,
                    'categoria_id' => $categoria
                ];

                $ligacao = ProdutoLinhaCategoria::where('produto_id', $produto->id)->where('linha_id', $linhaP->id)->where('categoria_id', $categoria)->first();
                if ($ligacao == null) {
                    ProdutoLinhaCategoria::create($data);
                }
            }

            return redirect()->route('produtos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Produto $produto)
    {
        try {
            $produto->delete();

            return redirect()->route('produtos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
