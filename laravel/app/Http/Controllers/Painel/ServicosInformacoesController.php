<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosInformacoesRequest;
use App\Models\Servico;
use App\Models\ServicoInformacao;
use Illuminate\Http\Request;

class ServicosInformacoesController extends Controller
{
    public function index(Servico $servico)
    {
        $informacoes = ServicoInformacao::servico($servico->id)->ordenados()->get();

        return view('painel.servico-ao-cliente.servicos.informacoes.index', compact('informacoes', 'servico'));
    }

    public function create(Servico $servico)
    {
        return view('painel.servico-ao-cliente.servicos.informacoes.create', compact('servico'));
    }

    public function store(ServicosInformacoesRequest $request, Servico $servico)
    {
        try {
            $input = $request->all();
            $input['servico_id'] = $servico->id;

            ServicoInformacao::create($input);

            return redirect()->route('servicos.informacoes.index', $servico->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Servico $servico, ServicoInformacao $informaco)
    {
        $informacao = $informaco;

        return view('painel.servico-ao-cliente.servicos.informacoes.edit', compact('informacao', 'servico'));
    }

    public function update(ServicosInformacoesRequest $request, Servico $servico, ServicoInformacao $informaco)
    {
        try {
            $input = $request->all();
            $input['servico_id'] = $servico->id;

            $informaco->update($input);

            return redirect()->route('servicos.informacoes.index', $servico->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Servico $servico, ServicoInformacao $informaco)
    {
        try {
            $informaco->delete();

            return redirect()->route('servicos.informacoes.index', $servico->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
