<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicoAoClienteRequest;
use App\Models\ServicoAoCliente;
use Illuminate\Http\Request;

class ServicoAoClienteController extends Controller
{
    public function index()
    {
        $servicoAoCliente = ServicoAoCliente::first();

        return view('painel.servico-ao-cliente.pagina.edit', compact('servicoAoCliente'));
    }

    public function update(ServicoAoClienteRequest $request, ServicoAoCliente $servicoAoCliente)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ServicoAoCliente::upload_capa();

            $servicoAoCliente->update($input);

            return redirect()->route('servico-ao-cliente.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
