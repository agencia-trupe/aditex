<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosRequest;
use App\Models\Servico;
use Illuminate\Http\Request;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::orderBy('id', 'asc')->get();

        return view('painel.servico-ao-cliente.servicos.index', compact('servicos'));
    }

    public function edit(Servico $servico)
    {
        return view('painel.servico-ao-cliente.servicos.edit', compact('servico'));
    }

    public function update(ServicosRequest $request, Servico $servico)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Servico::upload_capa();

            $servico->update($input);

            return redirect()->route('servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
