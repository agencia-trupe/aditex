<?php

namespace App\Http\Controllers;

use App\Models\Servico;
use App\Models\ServicoAoCliente;
use App\Models\ServicoImagem;
use App\Models\ServicoInformacao;
use Illuminate\Http\Request;

class ServicoAoClienteController extends Controller
{
    public function index()
    {
        $pagina = ServicoAoCliente::first();
        $servicos = Servico::orderBy('id', 'asc')->get();

        return view('frontend.servico-ao-cliente', compact('pagina', 'servicos'));
    }

    public function show($servico_slug)
    {
        $servico = Servico::where('slug', $servico_slug)->first();
        $imagens = ServicoImagem::where('servico_id', $servico->id)->ordenados()->get();
        $informacoes = ServicoInformacao::where('servico_id', $servico->id)->ordenados()->get();

        $servicos = Servico::orderBy('id', 'asc')->get();

        return view('frontend.servico-ao-cliente-show', compact('servico', 'imagens', 'informacoes', 'servicos'));
    }
}
