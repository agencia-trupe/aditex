<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\EmpresaInformacao;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();
        $informacoes = EmpresaInformacao::ordenados()->get();

        return view('frontend.empresa', compact('empresa', 'informacoes'));
    }
}
