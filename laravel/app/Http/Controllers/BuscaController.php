<?php

namespace App\Http\Controllers;

use App\Models\Aplicacao;
use App\Models\Banner;
use App\Models\Categoria;
use App\Models\Empresa;
use App\Models\EmpresaInformacao;
use App\Models\Home;
use App\Models\Linha;
use App\Models\Noticia;
use App\Models\PoliticaDePrivacidade;
use App\Models\Produto;
use App\Models\ProdutoArquivo;
use App\Models\ProdutosPagina;
use App\Models\Servico;
use App\Models\ServicoAoCliente;
use App\Models\ServicoInformacao;
use Illuminate\Http\Request;

class BuscaController extends Controller
{
    public function postPalavra(Request $request)
    {
        $termo = $request->busca;
        $resultados = [];

        if (!empty($termo)) {

            $busca_aplicacoes = Aplicacao::busca($termo)->get();
            foreach ($busca_aplicacoes as $dados) {
                $resultados['aplicacoes'][$dados->id]['id']     = $dados->id;
                $resultados['aplicacoes'][$dados->id]['link']   = route('produtos.filtro', ["0", $dados->slug]);
                $resultados['aplicacoes'][$dados->id]['menu']   = "PRODUTOS";
                $resultados['aplicacoes'][$dados->id]['titulo'] = $dados->titulo;
                $resultados['aplicacoes'][$dados->id]['frase']  = "Clique e veja os produtos relacionados à aplicação: " . $dados->titulo;
            }

            $busca_banners = Banner::busca($termo)->get();
            foreach ($busca_banners as $dados) {
                $resultados['banners'][$dados->id]['id']     = $dados->id;
                $resultados['banners'][$dados->id]['link']   = route('home');
                $resultados['banners'][$dados->id]['menu']   = "HOME";
                $resultados['banners'][$dados->id]['titulo'] = "Banners";
                $resultados['banners'][$dados->id]['frase']  = $dados->titulo . " | " . $dados->frase;
            }

            $busca_categorias = Categoria::busca($termo)->get();
            foreach ($busca_categorias as $dados) {
                $resultados['categorias'][$dados->id]['id']     = $dados->id;
                $resultados['categorias'][$dados->id]['link']   = route('produtos.filtro', [$dados->slug, "0"]);
                $resultados['categorias'][$dados->id]['menu']   = "PRODUTOS";
                $resultados['categorias'][$dados->id]['titulo'] = $dados->titulo;
                $resultados['categorias'][$dados->id]['frase']  = "Clique e veja os produtos relacionados à categoria: " . $dados->titulo;
            }

            $busca_empresa = Empresa::busca($termo)->get();
            foreach ($busca_empresa as $dados) {
                $resultados['empresa'][$dados->id]['id']     = $dados->id;
                $resultados['empresa'][$dados->id]['link']   = route('empresa');
                $resultados['empresa'][$dados->id]['menu']   = "EMPRESA";
                $resultados['empresa'][$dados->id]['titulo'] = "Informações";
                $resultados['empresa'][$dados->id]['frase']  = $dados->texto_historia . " | " . $dados->frase_estrutura . " | " . $dados->politica_qualidade . " | " . $dados->objetivos_qualidade . " | " . $dados->titulo_servicos . " | " . $dados->frase_servicos;
            }

            $busca_empresa_infos = EmpresaInformacao::busca($termo)->get();
            foreach ($busca_empresa_infos as $dados) {
                $resultados['empresa'][$dados->id]['id']     = $dados->id;
                $resultados['empresa'][$dados->id]['link']   = route('empresa') . "#info-" . $dados->id;
                $resultados['empresa'][$dados->id]['menu']   = "EMPRESA";
                $resultados['empresa'][$dados->id]['titulo'] = $dados->titulo;
                $resultados['empresa'][$dados->id]['frase']  = $dados->texto;
            }

            $busca_home = Home::busca($termo)->get();
            foreach ($busca_home as $dados) {
                $resultados['home'][$dados->id]['id']     = $dados->id;
                $resultados['home'][$dados->id]['link']   = route('home');
                $resultados['home'][$dados->id]['menu']   = "HOME";
                $resultados['home'][$dados->id]['titulo'] = "Informações";
                $resultados['home'][$dados->id]['frase']  = $dados->frase_segmentos . " | " . $dados->titulo_servicos . " | " . $dados->frase_servicos;
            }

            $busca_linhas = Linha::busca($termo)->get();
            foreach ($busca_linhas as $dados) {
                $resultados['linhas'][$dados->id]['id']     = $dados->id;
                $resultados['linhas'][$dados->id]['link']   = route('produtos.linha', $dados->slug);
                $resultados['linhas'][$dados->id]['menu']   = "PRODUTOS";
                $resultados['linhas'][$dados->id]['titulo'] = $dados->titulo;
                $resultados['linhas'][$dados->id]['frase']  = $dados->frase;
            }

            $busca_noticias = Noticia::busca($termo)->get();
            foreach ($busca_noticias as $dados) {
                $resultados['noticias'][$dados->id]['id']     = $dados->id;
                $resultados['noticias'][$dados->id]['link']   = route('noticias.showNoticia', $dados->slug);
                $resultados['noticias'][$dados->id]['menu']   = "NOTÍCIAS";
                $resultados['noticias'][$dados->id]['titulo'] = $dados->titulo;
                $resultados['noticias'][$dados->id]['frase']  = $dados->frase . " | " . $dados->texto;
            }

            $busca_politica = PoliticaDePrivacidade::busca($termo)->get();
            foreach ($busca_politica as $dados) {
                $resultados['politica'][$dados->id]['id']     = $dados->id;
                $resultados['politica'][$dados->id]['link']   = route('politica-de-privacidade');
                $resultados['politica'][$dados->id]['menu']   = "POLÍTICA DE PRIVACIDADE";
                $resultados['politica'][$dados->id]['titulo'] = "Informações";
                $resultados['politica'][$dados->id]['frase']  = $dados->texto;
            }

            $busca_produtos = Produto::busca($termo)->get();
            foreach ($busca_produtos as $dados) {
                $resultados['produtos'][$dados->id]['id']     = $dados->id;
                $resultados['produtos'][$dados->id]['link']   = route('produtos.showProduto', $dados->slug);
                $resultados['produtos'][$dados->id]['menu']   = "PRODUTOS";
                $resultados['produtos'][$dados->id]['titulo'] = $dados->titulo;
                $resultados['produtos'][$dados->id]['frase']  = $dados->subtitulo . " | " . $dados->descricao . " | " . $dados->embalagem . " | " . $dados->armazenamento;
            }

            $busca_produtos_arquivos = ProdutoArquivo::busca($termo)->get();
            foreach ($busca_produtos_arquivos as $dados) {
                $produto = Produto::where('id', $dados->produto_id)->first();
                $resultados['produtos'][$dados->id]['id']     = $dados->id;
                $resultados['produtos'][$dados->id]['link']   = route('produtos.showProduto', $produto->slug) . "#arquivos";
                $resultados['produtos'][$dados->id]['menu']   = "PRODUTOS";
                $resultados['produtos'][$dados->id]['titulo'] = $produto->titulo;
                $resultados['produtos'][$dados->id]['frase']  = "Veja mais sobre o produto relacionado à este arquivo: " . $dados->titulo_arquivo;
            }

            $busca_produtos_pg = ProdutosPagina::busca($termo)->get();
            foreach ($busca_produtos_pg as $dados) {
                $resultados['produtos'][$dados->id]['id']     = $dados->id;
                $resultados['produtos'][$dados->id]['link']   = route('produtos');
                $resultados['produtos'][$dados->id]['menu']   = "PRODUTOS";
                $resultados['produtos'][$dados->id]['titulo'] = "Informações";
                $resultados['produtos'][$dados->id]['frase']  = $dados->titulo_capa . " | " . $dados->frase;
            }

            $busca_servicos = Servico::busca($termo)->get();
            foreach ($busca_servicos as $dados) {
                $resultados['servicos'][$dados->id]['id']     = $dados->id;
                $resultados['servicos'][$dados->id]['link']   = route('servico-ao-cliente.show', $dados->slug);
                $resultados['servicos'][$dados->id]['menu']   = "SERVIÇOS";
                $resultados['servicos'][$dados->id]['titulo'] = $dados->titulo;
                $resultados['servicos'][$dados->id]['frase']  = $dados->subtitulo . " | " . $dados->texto;
            }

            $busca_servicos_pg = ServicoAoCliente::busca($termo)->get();
            foreach ($busca_servicos_pg as $dados) {
                $resultados['servicos'][$dados->id]['id']     = $dados->id;
                $resultados['servicos'][$dados->id]['link']   = route('servico-ao-cliente');
                $resultados['servicos'][$dados->id]['menu']   = "SERVIÇOS";
                $resultados['servicos'][$dados->id]['titulo'] = "Informações";
                $resultados['servicos'][$dados->id]['frase']  = $dados->frase_capa . " | " . $dados->texto . " | " . $dados->frase_texto;
            }

            $busca_servicos_infos = ServicoInformacao::busca($termo)->get();
            foreach ($busca_servicos_infos as $dados) {
                $servico = Servico::where('id', $dados->servico_id)->first();
                $resultados['servicos'][$dados->id]['id']     = $dados->id;
                $resultados['servicos'][$dados->id]['link']   = route('servico-ao-cliente.show', $servico->slug) . "#info-" . $dados->id;
                $resultados['servicos'][$dados->id]['menu']   = "SERVIÇOS";
                $resultados['servicos'][$dados->id]['titulo'] = $servico->titulo;
                $resultados['servicos'][$dados->id]['frase']  = $dados->titulo_info . " | " . $dados->frase_info;
            }
        } else {
            $resultados = [];
        }

        // dd($resultados);

        return view('frontend.busca', compact('termo', 'resultados'));
    }
}
