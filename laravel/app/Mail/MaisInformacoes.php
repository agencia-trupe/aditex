<?php

namespace App\Mail;

use App\Models\ContatoMaisInformacao;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MaisInformacoes extends Mailable
{
    use Queueable, SerializesModels;

    public $contato;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContatoMaisInformacao $contatoInfo)
    {
        $this->contato = $contatoInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[PRODUTO: ' . $this->contato['produto'] . ' - MAIS INFORMAÇÕES] ' . config('app.name'))->view('emails.mais-informacoes');
    }
}
