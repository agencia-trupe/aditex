<?php

namespace App\Mail;

use App\Models\TrabalheConosco as ModelsTrabalheConosco;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TrabalheConosco extends Mailable
{
    use Queueable, SerializesModels;

    public $contato;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ModelsTrabalheConosco $trabalheConosco)
    {
        $this->contato = $trabalheConosco;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[TRABALHE CONOSCO] ' . config('app.name'))->view('emails.trabalhe-conosco');
    }
}
