<?php

namespace App\Providers;

use App\Http\Controllers\HomeController;
use App\Models\AceiteDeCookies;
use App\Models\Aplicacao;
use App\Models\Categoria;
use App\Models\Configuracao;
use App\Models\Contato;
use App\Models\ContatoMaisInformacao;
use App\Models\Linha;
use App\Models\Servico;
use App\Models\TrabalheConosco;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('config', Configuracao::first());
        });

        View::composer('frontend.*', function ($view) {
            $view->with('contato', Contato::first());

            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        View::composer('frontend.layout*', function ($view) {
            $view->with('linhas', Linha::orderBy('titulo', 'asc')->get());
            $view->with('categorias', Categoria::orderBy('titulo', 'asc')->get());
            $view->with('categoriasFiltro', Categoria::orderBy('titulo', 'asc')->select('slug', 'titulo')->distinct()->get());
            $view->with('aplicacoes', Aplicacao::orderBy('titulo', 'asc')->get());
            $view->with('servicos', Servico::orderBy('id', 'asc')->get());

            // API DÓLAR
            $dataAnterior = date("m-d-Y", strtotime("-1 days")); // usando data anterior pra pegar valor

            for ($i = 0; $i < 10; $i++) {
                $cotacao = HomeController::getDolar($dataAnterior);
                if (empty($cotacao->value)) {
                    $dataAnterior = date_create_from_format('m-d-Y', $dataAnterior)->modify('-1 day')->format('m-d-Y');
                    $cotacao = HomeController::getDolar($dataAnterior);
                } else {
                    break;
                }
                if ($i > 10) {
                    break; // para não travar aplicação caso não retorne em 10 vezes.
                }
            }

            $valorCotacao = $cotacao->value[0]->cotacaoVenda;
            if (strlen($valorCotacao) < 6) {
                $valorCotacao = $valorCotacao . "0";
            }
            $dataCotacao = date("d/m");

            $view->with('valorCotacao', $valorCotacao);
            $view->with('dataCotacao', $dataCotacao);
        });

        View::composer('painel.layout.*', function ($view) {
            $view->with('contatosInfosNaoLidos', ContatoMaisInformacao::naoLidos()->count());
            $view->with('contatosCurriculosNaoLidos', TrabalheConosco::naoLidos()->count());
        });
    }
}
