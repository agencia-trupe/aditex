<?php

use App\Http\Controllers\BuscaController;
use App\Http\Controllers\ContatoController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NoticiasController;
use App\Http\Controllers\PoliticaDePrivacidadeController;
use App\Http\Controllers\ProdutosController;
use App\Http\Controllers\ServicoAoClienteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('filtro/{categoria_slug}', [HomeController::class, 'getItensFiltro'])->name('getItensFiltro');

Route::get('empresa', [EmpresaController::class, 'index'])->name('empresa');

Route::get('produtos', [ProdutosController::class, 'index'])->name('produtos');
Route::get('produtos/{linha_slug}', [ProdutosController::class, 'indexLinhas'])->name('produtos.linha');
Route::get('produtos/{linha_slug}/categorias/{categoria_slug}', [ProdutosController::class, 'indexLinhasCategorias'])->name('produtos.linha.categoria');
Route::get('produtos/produto/{produto_slug}', [ProdutosController::class, 'show'])->name('produtos.showProduto');
// Route::get('produtos/{produto_id}/arquivo/{arquivo_slug}', [ProdutosController::class, 'openArquivoPDF'])->name('produtos.arquivos');
Route::post('produtos/mais-informacoes/{produto}', [ProdutosController::class, 'postMaisInformacoes'])->name('produtos.mais-informacoes');
Route::get('produtos/filtro/{categoria_slug}/{aplicacao_slug}', [ProdutosController::class, 'filtroProdutos'])->name('produtos.filtro');

Route::get('servico-ao-cliente', [ServicoAoClienteController::class, 'index'])->name('servico-ao-cliente');
Route::get('servico-ao-cliente/{servico_slug}', [ServicoAoClienteController::class, 'show'])->name('servico-ao-cliente.show');

Route::get('noticias', [NoticiasController::class, 'index'])->name('noticias');
Route::get('noticias/{noticia_slug}', [NoticiasController::class, 'show'])->name('noticias.showNoticia');
// Route::get('noticias/{noticia_slug}/pdf', [NoticiasController::class, 'openPdf'])->name('noticias.pdf');

Route::get('contato', [ContatoController::class, 'index'])->name('contato');
Route::post('trabalhe-conosco', [ContatoController::class, 'postTrabalheConosco'])->name('trabalhe-conosco.post');
// Route::get('trabalhe-conosco/{id}', [ContatoController::class, 'openArquivoPDF'])->name('trabalhe-conosco.arquivo');
Route::get('politica-de-privacidade', [PoliticaDePrivacidadeController::class, 'index'])->name('politica-de-privacidade');
Route::post('busca', [BuscaController::class, 'postPalavra'])->name('busca.palavra');
Route::post('aceite-de-cookies', [HomeController::class, 'postCookies'])->name('aceite-de-cookies.post');

require __DIR__ . '/auth.php';
