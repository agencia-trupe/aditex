<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\AceiteDeCookiesController;
use App\Http\Controllers\Painel\AplicacoesController;
use App\Http\Controllers\Painel\BannersController;
use App\Http\Controllers\Painel\CategoriasController;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Http\Controllers\Painel\ContatosController;
use App\Http\Controllers\Painel\ContatosMaisInformacoesController;
use App\Http\Controllers\Painel\EmpresaController;
use App\Http\Controllers\Painel\EmpresaInformacoesController;
use App\Http\Controllers\Painel\HomeController;
use App\Http\Controllers\Painel\LinhasController;
use App\Http\Controllers\Painel\NoticiasController;
use App\Http\Controllers\Painel\PainelController;
use App\Http\Controllers\Painel\PoliticaDePrivacidadeController;
use App\Http\Controllers\Painel\ProdutosArquivosController;
use App\Http\Controllers\Painel\ProdutosController;
use App\Http\Controllers\Painel\ProdutosPaginaController;
use App\Http\Controllers\Painel\ServicoAoClienteController;
use App\Http\Controllers\Painel\ServicosController;
use App\Http\Controllers\Painel\ServicosImagensController;
use App\Http\Controllers\Painel\ServicosInformacoesController;
use App\Http\Controllers\Painel\TrabalheConoscoController;
use App\Http\Controllers\Painel\UsersController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'painel',
], function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');

    // PAINEL
    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('/', [PainelController::class, 'index'])->name('painel');
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);

        // configs, usuarios, etc
        Route::resource('usuarios', UsersController::class);
        Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
        Route::resource('politica-de-privacidade', PoliticaDePrivacidadeController::class)->only(['index', 'update']);
        Route::get('aceite-de-cookies', [AceiteDeCookiesController::class, 'index'])->name('aceite-de-cookies.index');

        // contatos
        Route::resource('contatos', ContatosController::class)->only(['index', 'update']);
        Route::get('mais-informacoes/{id}/toggle', [ContatosMaisInformacoesController::class, 'toggle'])->name('mais-informacoes.toggle');
        Route::get('trabalhe-conosco/{id}/toggle', [TrabalheConoscoController::class, 'toggle'])->name('trabalhe-conosco.toggle');
        Route::resource('mais-informacoes', ContatosMaisInformacoesController::class)->only(['index', 'show', 'destroy']);
        Route::resource('trabalhe-conosco', TrabalheConoscoController::class)->only(['index', 'show', 'destroy']);

        // home
        Route::resource('home', HomeController::class)->only(['index', 'update']);
        Route::resource('banners', BannersController::class);

        // empresa
        Route::resource('empresa', EmpresaController::class)->only(['index', 'update']);
        Route::resource('empresa-informacoes', EmpresaInformacoesController::class);

        // produtos 
        Route::resource('produtos-pagina', ProdutosPaginaController::class)->only(['index', 'update']);
        Route::resource('linhas', LinhasController::class);
        Route::resource('categorias', CategoriasController::class);
        Route::resource('aplicacoes', AplicacoesController::class);
        Route::resource('produtos', ProdutosController::class);
        Route::resource('produtos.arquivos', ProdutosArquivosController::class);

        // serviço ao cliente
        Route::resource('servico-ao-cliente', ServicoAoClienteController::class)->only(['index', 'update']);
        Route::resource('servicos', ServicosController::class)->only(['index', 'edit', 'update']);
        Route::get('servicos/{servico}/imagens/clear', [ServicosImagensController::class, 'clear'])->name('servicos.imagens.clear');
        Route::resource('servicos.imagens', ServicosImagensController::class);
        Route::resource('servicos.informacoes', ServicosInformacoesController::class);

        // noticias
        Route::resource('noticias', NoticiasController::class);

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });
    });
});
